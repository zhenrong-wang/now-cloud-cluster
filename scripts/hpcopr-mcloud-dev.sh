#!/bin/bash

MAXIMUM_CLUSTERS=5
current_user=`whoami`
# This scrip MUST be executed by the user hpc-now.
if [ $current_user != "hpc-now" ]; then
  echo -e "[ FATAL: ] You *MUST* create / switch to the user 'hpc-now' to operating a cluster. Exit now.\n"
  exit
fi

# This makes sure that an operator node can manage up to 5 clusters concurrently
if [[ -n "$1" && $1 = "new" ]]; then
  if [ ! -f /tmp/.secrets.txt ]; then
    echo -e "[ FATAL: ] Critical error. Please contact info@hpc-now.com. Exit now."
    exit
  else
    CURRENT_CLUSTERS=`ls -ll /home/hpc-now/ | grep now-cluster | wc -l`
    if [ $CURRENT_CLUSTERS -ge $((MAXIMUM_CLUSTERS)) ]; then
      echo -e "[ FATAL: ] Currently you already have $CURRENT_CLUSTERS clusters and reached the maximum number. Please destroy one or more cluster(s) and delete the related folders before creating new ones. Exit now.\n"
      exit
    else
      echo -e "[ -INFO- ] Currently you have $CURRENT_CLUSTERS clusters. Please be aware that you can have *up to* $MAXIMUM_CLUSTERS clusters concurrently.\n"
      NEW_CLUSTER_NUMBER=$((CURRENT_CLUSTERS+1))
      mkdir -p /home/hpc-now/now-cluster-$NEW_CLUSTER_NUMBER/vault
      mv /tmp/.secrets.txt /home/hpc-now/now-cluster-$NEW_CLUSTER_NUMBER/vault/
      echo -e "[ -DONE- ] The working directory of your new cluster: /home/hpc-now/now-cluster-$NEW_CLUSTER_NUMBER. Please go to that directory and run 'hpcopr init' to create a default cluster. Exit now.\n"
      exit
    fi
  fi
fi

# Restrict the workding directory, the working directory MUST be /home/hpc-now/now-cluster-###
curr_dir=`pwd`
curr_dir_up=`dirname $curr_dir`
curr_folder=`basename $curr_dir`
if [ $curr_dir_up != '/home/hpc-now' ]; then
  echo -e "[ FATAL: ] You are not in a valid working directory (i.e. /home/hpc-now/now-cluster-1), no critical operation is permitted.\nExit now.\n"
  exit
else
  folder_name_flag=`echo $curr_folder | awk -F'-' '{print NF}'`
  if [ $folder_name_flag -ne $((3)) ]; then
    echo -e "[ FATAL: ] You are not in a valid working directory (i.e. /home/hpc-now/now-cluster-1), no critical operation is permitted.\nExit now.\n"
    exit
  else
    first_c=`echo $curr_folder | awk -F'-' '{print $1}'`
    second_c=`echo $curr_folder | awk -F'-' '{print $2}'`
    last_c=`echo $curr_folder | awk -F'-' '{print $3}'`
    if [[ $first_c != 'now' || $second_c != 'cluster' ]]; then
      echo -e "[ FATAL: ] You are not in a valid working directory (i.e. /home/hpc-now/now-cluster-1), no critical operation is permitted.\nExit now.\n"
      exit
    else
      expr $last_c + 0 &>/dev/null
      if [ $? -ne 0 ]; then
        echo -e "[ FATAL: ] You are not in a valid working directory (i.e. /home/hpc-now/now-cluster-1), no critical operation is permitted.\nExit now.\n"
        exit
      else
        if [[ $last_c -lt $((1)) || $last_c -gt $((MAXIMUM_CLUSTERS)) ]]; then
          echo -e "[ FATAL: ] You are not in a valid working directory (i.e. /home/hpc-now/now-cluster-1), no critical operation is permitted.\nExit now.\n"
          exit
        fi
      fi  
    fi
  fi
fi

WDIR=$curr_dir
#STACKDIR=$WDIR/stack #For production environment, hidding the terraform files is extremely important. So we need to specify it to /etc/.stack/.stack
#STACKDIR=/etc/.stack/.stack
STACKDIR=/etc/js/ops/.stk/.stack-$curr_folder
VAULTDIR=$WDIR/vault
CONFDIR=$WDIR/conf
LOGDIR=$WDIR/log

# Clear .ssh/known_hosts
rm -rf /home/hpc-now/.ssh/known_hosts

# Global variable to determine the cloud env
CLOUD_FLAG=`cat $VAULTDIR/.secrets.txt | tail -n1`
if [[ $CLOUD_FLAG != 'CLOUD_A' && $CLOUD_FLAG != 'CLOUD_B' && $CLOUD_FLAG != 'CLOUD_C' ]]; then
  echo "[ FATAL: ] Your keyfile is missing. Please retry 'hpcopr' command. Exit now.\n"
  rm -rf $VAULTDIR/.secrets.txt
  exit
fi

if [ $CLOUD_FLAG = "CLOUD_A" ]; then
  CLOUD_STR="alicloud"
elif [ $CLOUD_FLAG = "CLOUD_B" ]; then
  CLOUD_STR="tencentcloud"
elif [ $CLOUD_FLAG = "CLOUD_C" ]; then
  CLOUD_STR="aws"
fi

# Global variable to determine the number of compute nodes
if [ -f $STACKDIR/terraform.tfstate ]; then
  if [[ $CLOUD_STR = 'alicloud' || $CLOUD_STR = 'tencentcloud' ]]; then
    COMPUTE_NODE_NUM=`cat $STACKDIR/terraform.tfstate | grep "\"instance_name\": \"compute" | wc -l`
  elif [ $CLOUD_STR = 'aws' ]; then
    COMPUTE_NODE_NUM=`cat $STACKDIR/terraform.tfstate | grep "\"name\": \"compute" | wc -l`
  fi
fi

# START:the script
URL_ROOT_ALI=https://gitee.com/zhenrong-wang/now-cloud-cluster/raw/main/tf-templates-alicloud/
URL_ROOT_QCLOUD=https://gitee.com/zhenrong-wang/now-cloud-cluster/raw/main/tf-templates-qcloud/
URL_ROOT_AWS=https://gitee.com/zhenrong-wang/now-cloud-cluster/raw/main/tf-templates-aws/

time1=$(date)
echo -e "   /HPC->  Welcome to HPC_NOW Cluster Operator! \n \\/ ->NOW  ${time1}\n\nVersion: 0.22.0810\n"

# Check Parameters
if [[ -n "$1" && $1 != "help" && $1 != "init" && $1 != "rmc" && $1 != "rmcdb" && $1 != "delmc" && $1 != "delmcdb" && $1 != "delc" && $1 != "resume" && $1 != "emptytrash" && $1 != "addc" && $1 != "graph" && $1 != "shutdownc" && $1 != "allupc" && $1 != "reconfc" && $1 != "updateconf" && $1 != "destroy" && $1 != "repair" && $1 != "conf" && $1 != "monthpay" ]]; then
  echo -e "$1 is not a valid parameter, please run 'help' to check the usage. Thanks.\nExit now.\n"
  exit
fi

# Check the current directory
function envcheck() {
  currentdir=`pwd`
  if [[ $currentdir != $WDIR ]]; then
    echo -e "[ FATAL: ] You are not in the working directory $WDIR, no critical operation is permitted. Please go to the $WDIR first.\nExit now.\n"
    exit
  fi
  if [[ ! -d stack || ! -d vault ]]; then
    if [[ ! -f tf_prep.conf && ! -f $CONFDIR/tf_prep.conf ]]; then
      echo -e "[ FATAL: ] It seems your cluster is not in place. Please run 'init' first.\n[ FATAL: ] Exit now.\n"
      exit
    fi
  fi
}

# Remote execute hpcmgr connect command
function rexec_connect() {
  root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
  master_address=`cat $STACKDIR/currentstate | head -n1`
  if [ ! -n $1 ]; then
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "hpcmgr connect"
  elif [[ -n $1 && $1 -eq $((1)) ]]; then
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "echo \"hpcmgr connect\" | at now + 1 minute"
  elif [[ -n $1 && $1 -gt $((1)) ]]; then
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "echo \"hpcmgr connect\" | at now + $1 minutes"
  else
    exit 1
  fi
}

# Remote execute hpcmgr all command
function rexec_all() {
  root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
  master_address=`cat $STACKDIR/currentstate | head -n1`
  if [ ! -n $1 ]; then
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "hpcmgr all"
  elif [[ -n $1 && $1 -eq $((1)) ]]; then
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "echo \"hpcmgr all\" | at now + 1 minute"
  elif [[ -n $1 && $1 -gt $((1)) ]]; then
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "echo \"hpcmgr all\" | at now + $1 minutes"
  else
    exit 1
  fi  
}

# Remote execute hpcmgr clear command
function rexec_clear() {
  root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
  master_address=`cat $STACKDIR/currentstate | head -n1`
  if [ ! -n $1 ]; then
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "hpcmgr clear"
  elif [[ -n $1 && $1 -eq $((1)) ]]; then
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "echo \"hpcmgr clear\" | at now + 1 minute"
  elif [[ -n $1 && $1 -gt $((1)) ]]; then
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "echo \"hpcmgr clear\" | at now + $1 minutes"
  else
    exit 1
  fi  
}

########## A small tool to get the IP addresses ########################
function getstate() {
  if [ -f $STACKDIR/currentstate ]; then
    rm -rf $STACKDIR/currentstate 
  fi
  touch $STACKDIR/currentstate
  
  if [[ $CLOUD_STR = 'tencentcloud' || $CLOUD_STR = 'alicloud' ]]; then
    node_num_gs=`cat $STACKDIR/terraform.tfstate | grep "\"instance_name\": \"compute" | wc -l`
    sed -n '/\"instance_name\": \"master\"/,+30p' $STACKDIR/terraform.tfstate | grep -w public_ip | awk -F"\"" '{print $4}' >> $STACKDIR/currentstate
    sed -n '/\"instance_name\": \"master\"/,+30p' $STACKDIR/terraform.tfstate | grep -w private_ip | awk -F"\"" '{print $4}' >> $STACKDIR/currentstate
    if [ $CLOUD_STR = 'tencentcloud' ]; then
      sed -n '/\"instance_name\": \"master\"/,+30p' $STACKDIR/terraform.tfstate | grep -w instance_status | awk -F"\"" '{print $4}' >> $STACKDIR/currentstate
      sed -n '/\"instance_name\": \"database\"/,+30p' $STACKDIR/terraform.tfstate | grep -w instance_status | awk -F"\"" '{print $4}' >> $STACKDIR/currentstate
    elif [ $CLOUD_STR = 'alicloud' ]; then
      sed -n '/\"instance_name\": \"master\"/,+30p' $STACKDIR/terraform.tfstate | grep -w status | awk -F"\"" '{print $4}' >> $STACKDIR/currentstate
      sed -n '/\"instance_name\": \"database\"/,+30p' $STACKDIR/terraform.tfstate | grep -w status | awk -F"\"" '{print $4}' >> $STACKDIR/currentstate
    fi
    for i in $(seq 1 $node_num_gs)
    do
      sed -n '/\"instance_name\": \"compute'$i'\"/,+30p' $STACKDIR/terraform.tfstate | grep -w private_ip | awk -F"\"" '{print $4}' >> $STACKDIR/currentstate
      if [ $CLOUD_STR = 'tencentcloud' ]; then
        sed -n '/\"instance_name\": \"compute'$i'\"/,+30p' $STACKDIR/terraform.tfstate | grep -w instance_status | awk -F"\"" '{print $4}' >> $STACKDIR/currentstate
      elif [ $CLOUD_STR = 'alicloud' ]; then
        sed -n '/\"instance_name\": \"compute'$i'\"/,+30p' $STACKDIR/terraform.tfstate | grep -w status | awk -F"\"" '{print $4}' >> $STACKDIR/currentstate
      elif [ $CLOUD_STR = 'aws' ]; then
        sed -n '/\"name\": \"compute'$i'\"/,+60p' $STACKDIR/terraform.tfstate | grep -w instance_state | awk -F"\"" '{print $4}' >> $STACKDIR/currentstate
      fi
    done
  elif [ $CLOUD_STR = 'aws' ]; then
    node_num_gs=`cat $STACKDIR/terraform.tfstate | grep "\"name\": \"compute" | wc -l`
    sed -n '/\"name\": \"eip_master\"/,+30p' $STACKDIR/terraform.tfstate | grep -w public_ip | awk -F"\"" '{print $4}'>> $STACKDIR/currentstate
    sed -n '/\"name\": \"eni_master\"/,+60p' $STACKDIR/terraform.tfstate | grep -w private_ip | awk -F"\"" '{print $4}' >> $STACKDIR/currentstate
    sed -n '/\"name\": \"master\"/,+60p' $STACKDIR/terraform.tfstate | grep -w instance_state | awk -F"\"" '{print $4}' >> $STACKDIR/currentstate
    sed -n '/\"name\": \"database\"/,+60p' $STACKDIR/terraform.tfstate | grep -w instance_state | awk -F"\"" '{print $4}' >> $STACKDIR/currentstate
    for i in $(seq 1 $node_num_gs)
    do
      sed -n '/\"name\": \"eni_compute'$i'\"/,+60p' $STACKDIR/terraform.tfstate | grep -w private_ip | awk -F"\"" '{print $4}' >> $STACKDIR/currentstate
      sed -n '/\"name\": \"compute'$i'\"/,+60p' $STACKDIR/terraform.tfstate | grep -w instance_state | awk -F"\"" '{print $4}' >> $STACKDIR/currentstate
    done
  fi
}  

########## A small tool to show the graph of the cluster ###############
function graph(){
  if [[ ! -d $STACKDIR || ! -f $STACKDIR/terraform.tfstate ]]; then
    echo -e "[WARNING:] It seems the cluster is not ready for use. Please run 'init' first.\n"
    exit
  fi
  if [[ $CLOUD_STR = 'alicloud' || $CLOUD_STR = 'tencentcloud' ]]; then
    cat $STACKDIR/terraform.tfstate | grep "\"instance_name\": \"master\"" >> /dev/null
  elif [ $CLOUD_STR = 'aws' ]; then
    cat $STACKDIR/terraform.tfstate | grep "\"name\": \"master\"" >> /dev/null
  fi
  if [ $? -ne 0 ]; then
    echo -e "[WARNING:] It seems the cluster has no db or master nodes. Please run 'init' first.\n"
    exit
  else
    if [[ $CLOUD_STR = 'alicloud' || $CLOUD_STR = 'tencentcloud' ]]; then
      node_num=`cat $STACKDIR/terraform.tfstate | grep "\"instance_name\": \"compute" | wc -l`
    elif [ $CLOUD_STR = 'aws' ]; then
      node_num=`cat $STACKDIR/terraform.tfstate | grep "\"name\": \"compute" | wc -l`
    fi
    if [[ $CLOUD_STR = 'alicloud' || $CLOUD_STR = 'tencentcloud' ]]; then
      node_config=`cat $STACKDIR/compute_template | grep instance_type | awk '{print $3}' | awk -F"." '{print $3}'`
    elif [ $CLOUD_STR = 'aws' ]; then
      cat $STACKDIR/compute_template | grep "cpu_threads_per_core = 1" >> /dev/null 2>&1
      if [ $? -eq 0 ]; then
        node_config_fake=`cat $STACKDIR/compute_template | grep instance_type | awk '{print $3}' | awk -F"." '{print $3}'`
        number_of_vcpu=`echo $node_config_fake | awk -F"c" '{print $1}' | sed 's/[a-z]//g'`
        cpu_core_num=$((number_of_vcpu/2))
        node_config_raw=`echo $node_config_fake | sed "s@$number_of_vcpu@$cpu_core_num@g"`
        node_config=$node_config_raw--*HT-OFF*
      else
        node_config=`cat $STACKDIR/compute_template | grep instance_type | awk '{print $3}' | awk -F"." '{print $3}'`
      fi
    fi 
    getstate
    master_public_ip=`sed -n 1p $STACKDIR/currentstate`
    master_private_ip=`sed -n 2p $STACKDIR/currentstate`
    master_state=`sed -n 3p $STACKDIR/currentstate`
    db_state=`sed -n 4p $STACKDIR/currentstate`
    head_str="MASTER_NODE""($master_public_ip,$master_state)""<---->|"
    echo -e "HPC-NOW CLUSTER GRAPH:"
    head_length=${#head_str}
    spaces=`seq -s ' ' $head_length`
    new_spaces=`echo $spaces | sed 's/[0-9]//g'`
    echo -e "$new_spaces|<--->DB_NODE($db_state)\n$head_str"
    for i in $(seq 1 $node_num )
    do
      ip_line=$((3+i*2))
      state_line=$((4+i*2))
      compute_private_ip=`sed -n ${ip_line}p $STACKDIR/currentstate`
      compute_state=`sed -n ${state_line}p $STACKDIR/currentstate`
      echo -e "$new_spaces|<--->COMPUTE_NODE$i($node_config,$compute_private_ip,$compute_state)"
    done
    echo -e ""
  fi
}

########## Show the graph #################
if [[ -n "S1" && $1 = "graph" ]]; then
  envcheck
  graph
  echo -e "HPC NOW, START:now ... to infinity!"
  exit
fi

######### Show the help info #################
if [[ -n $1 && $1 = "help" ]]; then
  echo -e "Usage: hpcopr command_name param1\n"
  echo -e "Commands:\n"
  echo -e "0 . new       - Create a new cluster.\n1 . init      - (Re)initialize the whole cluster.\n2 . help      - show this information.\n3 . graph     - show the topology of your cluster.\n4 . rmc       - reinitialize the master and compute nodes.\n5 . rmcdb     - reinitialize the master, compute, and database nodes.\n6 . delmc     - delete the master and compute nodes from the cluster.\n7 . delmcdb   - delete the master, compute, and database nodes from the cluster.\n8 . delc      - delete all (if param1 is 'all') or part of (if param1 is a number less than the number of all the compute nodes) of the compute nodes.\n9 . addc      - add compute nodes to current cluster, number is specified by param1.\n10. shutdownc - shutdown all (if param1 is 'all') or part (if param1 is a number less than the number of all the compute nodes) of the compuute nodes.\n11. allupc    - turn on all the compute nodes.\n12. reconfc   - reconfigure the compute nodes (# of the cores and memory capacity).\n13. destroy   - destroy the whole cluster.\n14. conf      - download configuration template to modify and initialize cluster.\n15. monthpay  - change the payment mode from ondemand to monthly payment [ IRREVERSABLE!!! PLEASE MAKE SURE YOU DO WANT TO DO THIS OPERATION ].\n"
  echo -e "HPC NOW, START:now ... to infinity!"
  exit
fi
######## Define the log ####################
logfile=$LOGDIR/tf_prep.log
logfile_raw=$STACKDIR/tf_prep.log

######## Create the trashbin folder ########
# Make sure that there are only recent 5 records are stored
if [ ! -d $WDIR/.trashbin ]; then
  mkdir -p $WDIR/.trashbin
fi
trashbin_subfolders=$((`ls -lt $WDIR/.trashbin/ | wc -l`-1))
if [ $((trashbin_subfolders)) -gt $((5)) ]; then
  cd $WDIR/.trashbin && ls -lt | tail -n$((trash_subfolders-5)) | awk '{print $9}' | xargs rm -rf
  cd $WDIR
fi

####### Empty the trashbin #################
if [[ -n $1 && $1 = "emptytrash" ]]; then
  envcheck
  echo -e "[WARNING:] All the previously deleted resources will be removed permanently. "
  rm -rf $WDIR/.trashbin/*
  exit
fi

if [[ -n $1 && $1 = "repair" ]]; then
  envcheck
  ls -ll $STACKDIR/*.tf >> /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo -e "[ FATAL: ] Nothing to be repaired. You may need to run the command 'hpcopr init' first. Exit now.\n"
    exit
  fi
  echo -e "[ STEP 1 ] Repairing the cluster now, this may take seconds ... "
  cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
  getstate
  if [ $? -ne 0 ]; then
    echo -e "[WARNING:] Operation failed. Please check the ${logfile} for details."
    exit
  fi
  echo -e "[ STEP 2 ] Remote executing now ... " && rexec_connect 7 && rexec_all 8
  echo -e "[ -DONE- ] The cluster is repaired.\n"
  echo -e "[ -INFO- ] AFTER OPERATION:"
  graph
  exit
fi

if [[ -n $1 && $1 = "rmc" ]]; then
  envcheck
  graph
  REGION_ID=`cat $CONFDIR/tf_prep.conf | grep REGION_ID | cut -c 23-`  
  #confirmation
  echo -e "[ START: ] Reinitializing the master and compute nodes now ..."
  echo -e "[ STEP 1 ] Removing the master and compute node(s) ... "
  mkdir -p $WDIR/.tfpreptmp
  if [ -f $STACKDIR/hpc_stack_master.tf ]; then
    mv $STACKDIR/hpc_stack_master.tf $WDIR/.tfpreptmp/hpc_stack_master.tf.1
  fi
  for i in $(seq 1 $COMPUTE_NODE_NUM )
  do
    if [ -f $STACKDIR/hpc_stack_compute${i}.tf ]; then
      mv $STACKDIR/hpc_stack_compute${i}.tf $WDIR/.tfpreptmp/hpc_stack_compute${i}.tf.1
    fi
  done  
  cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
  getstate
  echo -e "[ STEP 1 ] The old master and compute node(s) are removed."
  echo -e "[ STEP 2 ] Allocating new master and compute node(s) now ..."
  if [ -f $WDIR/.tfpreptmp/hpc_stack_master.tf.1 ]; then
    mv $WDIR/.tfpreptmp/hpc_stack_master.tf.1 $STACKDIR/hpc_stack_master.tf
  fi
  for i in $(seq 1 $COMPUTE_NODE_NUM )
  do
    if [ -f $WDIR/.tfpreptmp/hpc_stack_compute${i}.tf.1 ]; then
      mv $WDIR/.tfpreptmp/hpc_stack_compute${i}.tf.1 $STACKDIR/hpc_stack_compute${i}.tf
    fi
  done
  cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
  getstate
  rm -rf $WDIR/.tfpreptmp
  echo -e "[ STEP 2 ] New master and compute node(s) are allocated."
  echo -e "[ STEP 3 ] Reinitializing the master and compute node(s) in progress. This step usually needs 7 minutes.\n[ -INFO- ] After 7 minutes of initialization, you can log in the master node and manage your cluster.\n"
  if [ $CLOUD_STR = 'aws' ]; then
    for i in $( seq 1 120)
    do
	    sleep 1
    done
    cat $CONF_FILE | grep REGION_ID | cut -c 23- | grep cn >> /dev/null 2>&1
    if [ $? -eq 0 ]; then
      REGION_FLAG=cn_regions
    else
      REGION_FLAG=global_regions
    fi
    bucket_name=`cat $STACKDIR/terraform.tfstate | grep -w \"bucket\" | head -n1 | awk -F"\"" '{print $4}'`
    bucket_secret_id=`sed -n '/aws_iam_access_key/,+15p' $STACKDIR/terraform.tfstate | grep -w id | awk -F"\"" '{print $4}'`
    bucket_access_secret=`sed -n '/aws_iam_access_key/,+15p' $STACKDIR/terraform.tfstate | grep -w secret | awk -F"\"" '{print $4}'`
    root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
    master_address=`cat $STACKDIR/currentstate | head -n1`
    if [ $REGION_FLAG = 'cn_regions' ]; then
      s3endpoint=s3.$REGION_ID.amazonaws.com.cn
      sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "wget ${URL_ROOT_AWS}s3cfg.txt -O /root/.s3cfg -q && sed -i 's#BLANK_ACCESS_KEY#$bucket_secret_id#g' /root/.s3cfg && sed -i 's#BLANK_SECRET_KEY_ID#$bucket_access_secret#g' /root/.s3cfg && sed -i 's#DEFAULT_REGION#$REGION_ID#g' /root/.s3cfg && sed -i 's#DEFAULT_ENDPOINT#$s3endpoint#g' /root/.s3cfg && chmod 600 /root/.s3cfg"
    else
      s3endpoint=s3.amazonaws.com
      sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "wget ${URL_ROOT_AWS}s3cfg.txt -O /root/.s3cfg -q && sed -i 's#BLANK_ACCESS_KEY#$bucket_secret_id#g' /root/.s3cfg && sed -i 's#BLANK_SECRET_KEY_ID#$bucket_access_secret#g' /root/.s3cfg && sed -i 's#DEFAULT_REGION#$REGION_ID#g' /root/.s3cfg && sed -i 's#DEFAULT_ENDPOINT#$s3endpoint#g' /root/.s3cfg && sed -i 's#amazonaws.com.cn##amazonaws.com#g' /root/.s3cfg && chmod 600 /root/.s3cfg"
    fi
  elif [ $CLOUD_STR = 'alicloud' ]; then
    for i in $( seq 1 15)
    do
	    sleep 1
    done
    root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
    master_address=`cat $STACKDIR/currentstate | head -n1`
    bucket_secret_id=`cat $STACKDIR/bucket_secrets.txt  | grep AccessKeyId | awk -F"\"" '{print $4}'`
    bucket_access_secret=`cat $STACKDIR/bucket_secrets.txt  | grep AccessKeySecret | awk -F"\"" '{print $4}'`
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "wget ${URL_ROOT_ALI}.ossutilconfig -O /root/.ossutilconfig -q && sed -i 's#BLANK_ACCESS_KEY#$bucket_secret_id#g' /root/.ossutilconfig && sed -i 's#BLANK_SECRET_KEY#$bucket_access_secret#g' /root/.ossutilconfig && sed -i 's#DEFAULT_REGION#$REGION_ID#g' /root/.ossutilconfig && chmod 644 /root/.ossutilconfig"
  elif [ $CLOUD_STR = 'tencentcloud' ]; then
    bucket_name=`cat $STACKDIR/terraform.tfstate | grep -w \"bucket\" | head -n1 | awk -F"\"" '{print $4}'`
    bucket_secret_id=`cat $STACKDIR/terraform.tfstate | grep -w secret_id | awk -F"\"" '{print $4}'`
    bucket_access_secret=`cat $STACKDIR/terraform.tfstate | grep -w secret_key | awk -F"\"" '{print $4}'`
    root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
    master_address=`cat $STACKDIR/currentstate | head -n1`
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "wget ${URL_ROOT_QCLOUD}cos.conf -O /root/.cos.conf -q && sed -i 's#BLANK_ACCESS_KEY#$bucket_secret_id#g' /root/.cos.conf && sed -i 's#BLANK_SECRET_KEY#$bucket_access_secret#g' /root/.cos.conf && sed -i 's#DEFAULT_REGION#$REGION_ID#g' /root/.cos.conf && sed -i 's#BLANK_BUCKET_NAME#$bucket_name#g' /root/.cos.conf && chmod 644 /root/.cos.conf"
  fi
  rexec_connect 7 && rexec_all 8
  echo -e "[ -INFO- ] AFTER OPERATION:"
  graph
  exit
fi

if [[ -n $1 && $1 = rmcdb ]]; then
  envcheck
  graph
  #confirmation
  REGION_ID=`cat $CONFDIR/tf_prep.conf | grep REGION_ID | cut -c 23-`
  echo -e "[ START: ] Reinitializing the master, compute and database nodes now ..."
  echo -e "[ STEP 1 ] Removing the master, compute and database nodes ... "
  mkdir -p $WDIR/.tfpreptmp
  mv $STACKDIR/hpc_stack_database.tf $WDIR/.tfpreptmp/hpc_stack_database.tf.1
  mv $STACKDIR/hpc_stack_master.tf $WDIR/.tfpreptmp/hpc_stack_master.tf.1
  for i in $(seq 1 $COMPUTE_NODE_NUM )
  do
    mv $STACKDIR/hpc_stack_compute${i}.tf $WDIR/.tfpreptmp/hpc_stack_compute${i}.tf.1
  done  
  cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
  getstate
  echo -e "[ STEP 1 ] The old nodes are removed."
  echo -e "[ STEP 2 ] Allocating new master, compute and database nodes now ..."
  mv $WDIR/.tfpreptmp/hpc_stack_database.tf.1 $STACKDIR/hpc_stack_database.tf
  mv $WDIR/.tfpreptmp/hpc_stack_master.tf.1 $STACKDIR/hpc_stack_master.tf
  for i in $(seq 1 $COMPUTE_NODE_NUM )
  do
    mv $WDIR/.tfpreptmp/hpc_stack_compute${i}.tf.1 $STACKDIR/hpc_stack_compute${i}.tf
  done
  cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
  getstate
  rm -rf $WDIR/.tfpreptmp
  echo -e "[ STEP 2 ] New master, compute and database nodes are allocated."
  echo -e "[ STEP 3 ] Reinitializing the master, compute and database nodes in progress. This step usually needs 7 minutes.\n[ -INFO- ] After 7 minutes of initialization, you can log in the master node and manage your cluster.\n"
  if [ $CLOUD_STR = 'aws' ]; then
    for i in $( seq 1 120)
    do
	    sleep 1
    done
        cat $CONF_FILE | grep REGION_ID | cut -c 23- | grep cn >> /dev/null 2>&1
    if [ $? -eq 0 ]; then
      REGION_FLAG=cn_regions
    else
      REGION_FLAG=global_regions
    fi
    bucket_name=`cat $STACKDIR/terraform.tfstate | grep -w \"bucket\" | head -n1 | awk -F"\"" '{print $4}'`
    bucket_secret_id=`sed -n '/aws_iam_access_key/,+15p' $STACKDIR/terraform.tfstate | grep -w id | awk -F"\"" '{print $4}'`
    bucket_access_secret=`sed -n '/aws_iam_access_key/,+15p' $STACKDIR/terraform.tfstate | grep -w secret | awk -F"\"" '{print $4}'`
    root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
    master_address=`cat $STACKDIR/currentstate | head -n1`
    if [ $REGION_FLAG = 'cn_regions' ]; then
      s3endpoint=s3.$REGION_ID.amazonaws.com.cn
      sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "wget ${URL_ROOT_AWS}s3cfg.txt -O /root/.s3cfg -q && sed -i 's#BLANK_ACCESS_KEY#$bucket_secret_id#g' /root/.s3cfg && sed -i 's#BLANK_SECRET_KEY_ID#$bucket_access_secret#g' /root/.s3cfg && sed -i 's#DEFAULT_REGION#$REGION_ID#g' /root/.s3cfg && sed -i 's#DEFAULT_ENDPOINT#$s3endpoint#g' /root/.s3cfg && chmod 600 /root/.s3cfg"
    else
      s3endpoint=s3.amazonaws.com
      sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "wget ${URL_ROOT_AWS}s3cfg.txt -O /root/.s3cfg -q && sed -i 's#BLANK_ACCESS_KEY#$bucket_secret_id#g' /root/.s3cfg && sed -i 's#BLANK_SECRET_KEY_ID#$bucket_access_secret#g' /root/.s3cfg && sed -i 's#DEFAULT_REGION#$REGION_ID#g' /root/.s3cfg && sed -i 's#DEFAULT_ENDPOINT#$s3endpoint#g' /root/.s3cfg && sed -i 's#amazonaws.com.cn##amazonaws.com#g' /root/.s3cfg && chmod 600 /root/.s3cfg"
    fi
  elif [ $CLOUD_STR = 'alicloud' ]; then
    for i in $( seq 1 15)
    do
	    sleep 1
    done
    root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
    master_address=`cat $STACKDIR/currentstate | head -n1`
    bucket_secret_id=`cat $STACKDIR/bucket_secrets.txt  | grep AccessKeyId | awk -F"\"" '{print $4}'`
    bucket_access_secret=`cat $STACKDIR/bucket_secrets.txt  | grep AccessKeySecret | awk -F"\"" '{print $4}'`
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "wget ${URL_ROOT_ALI}.ossutilconfig -O /root/.ossutilconfig -q && sed -i 's#BLANK_ACCESS_KEY#$bucket_secret_id#g' /root/.ossutilconfig && sed -i 's#BLANK_SECRET_KEY#$bucket_access_secret#g' /root/.ossutilconfig && sed -i 's#DEFAULT_REGION#$REGION_ID#g' /root/.ossutilconfig && chmod 644 /root/.ossutilconfig"
  elif [ $CLOUD_STR = 'tencentcloud' ]; then
    bucket_name=`cat $STACKDIR/terraform.tfstate | grep -w \"bucket\" | head -n1 | awk -F"\"" '{print $4}'`
    bucket_secret_id=`cat $STACKDIR/terraform.tfstate | grep -w secret_id | awk -F"\"" '{print $4}'`
    bucket_access_secret=`cat $STACKDIR/terraform.tfstate | grep -w secret_key | awk -F"\"" '{print $4}'`
    root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
    master_address=`cat $STACKDIR/currentstate | head -n1`
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "wget ${URL_ROOT_QCLOUD}cos.conf -O /root/.cos.conf -q && sed -i 's#BLANK_ACCESS_KEY#$bucket_secret_id#g' /root/.cos.conf && sed -i 's#BLANK_SECRET_KEY#$bucket_access_secret#g' /root/.cos.conf && sed -i 's#DEFAULT_REGION#$REGION_ID#g' /root/.cos.conf && sed -i 's#BLANK_BUCKET_NAME#$bucket_name#g' /root/.cos.conf && chmod 644 /root/.cos.conf"
  fi
  rexec_connect 7 && rexec_all 8
  echo -e "[ -INFO- ] AFTER OPERATION:"
  graph
  exit
fi

if [[ -n $1 && $1 = "delmc" ]]; then
  envcheck
  graph
  #confirmation
  echo -e "[ START: ] Deleting the master and compute nodes now ..."
  time_curr=`date "+%Y-%m-%d-%H:%M:%S"`
  mkdir -p $WDIR/.trashbin/$time_curr
  mv $STACKDIR/hpc_stack_master.tf $WDIR/.trashbin/$time_curr/hpc_stack_master.tf.1
  if [ $CLOUD_STR = 'aws' ]; then
    mv $STACKDIR/hpc_stack_master_eni.tf $WDIR/.trashbin/$time_curr/hpc_stack_master_eni.tf.1
  fi
  for i in $(seq 1 $COMPUTE_NODE_NUM )
  do
    mv $STACKDIR/hpc_stack_compute${i}.tf $WDIR/.trashbin/$time_curr/hpc_stack_compute${i}.tf.1
    if [ $CLOUD_STR = 'aws' ]; then
      mv $STACKDIR/hpc_stack_compute${i}_eni.tf $WDIR/.trashbin/$time_curr/hpc_stack_compute${i}_eni.tf.1
    fi
  done  
  cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
  if [ $? -eq 0 ]; then
    rm -rf $STACKDIR/currentstate
    rm -rf $STACKDIR/compute_template
    if [ $CLOUD_STR = 'aws' ]; then
      rm -rf $STACKDIR/compute_eni_template
    fi
  fi
  echo -e "[ -DONE- ] Deleted the master and compute nodes. You need to run the command 'hpcopr init' to rebuild them. Exit now.\n"
  exit
fi

if [[ -n $1 && $1 = "delmcdb" ]]; then
  envcheck
  graph
  #confirmation
  echo -e "[ START: ] Deleting the master, compute and database nodes now ..."
  time_curr=`date "+%Y-%m-%d-%H:%M:%S"`
  mkdir -p $WDIR/.trashbin/$time_curr
  mv $STACKDIR/hpc_stack_master.tf $WDIR/.trashbin/$time_curr/hpc_stack_master.tf.1
  mv $STACKDIR/hpc_stack_database.tf $WDIR/.trashbin/$time_curr/hpc_stack_database.tf.1
  if [ $CLOUD_STR = 'aws' ]; then
    mv $STACKDIR/hpc_stack_master_eni.tf $WDIR/.trashbin/$time_curr/hpc_stack_master_eni.tf.1
    mv $STACKDIR/hpc_stack_database_eni.tf $WDIR/.trashbin/$time_curr/hpc_stack_database_eni.tf.1
  fi
  for i in $(seq 1 $COMPUTE_NODE_NUM )
  do
    mv $STACKDIR/hpc_stack_compute${i}.tf $WDIR/.trashbin/$time_curr/hpc_stack_compute${i}.tf.1
    if [ $CLOUD_STR = 'aws' ]; then
      mv $STACKDIR/hpc_stack_compute${i}_eni.tf $WDIR/.trashbin/$time_curr/hpc_stack_compute${i}_eni.tf.1
    fi
  done  
  cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
  if [ $? -eq 0 ]; then
    rm -rf $STACKDIR/currentstate
    rm -rf $STACKDIR/compute_template
    if [ $CLOUD_STR = 'aws' ]; then
      rm -rf $STACKDIR/compute_eni_template
    fi
  fi
  echo -e "[ -DONE- ] Deleted the master, compute and database nodes. You need to run the command 'hpcopr init' to rebuild them.\n"
  exit
fi

if [[ -n $1 && $1 = "delc" ]]; then
  envcheck
  if [ $COMPUTE_NODE_NUM -eq $((0)) ]; then
    echo -e "[ FATAL: ] There is no compute nodes in current cluster, nothing deleted. Exit now.\n"
    exit
  fi
  graph
  if [ ! -n "$2" ]; then
    echo -e "[ FATAL: ] You need to specify to delete all or some node(s).\nExit now.\n"
    exit
  fi
  if [[ -n "$2" && $2 = 'all' ]]; then
    echo -e "[ STEP 1 ] Deleting all the compute nodes ... "
    #confirmation
    time_curr=`date "+%Y-%m-%d-%H:%M:%S"`
    mkdir -p $WDIR/.trashbin/$time_curr
    for i in $(seq 1 $COMPUTE_NODE_NUM )
    do
      mv $STACKDIR/hpc_stack_compute${i}.tf $WDIR/.trashbin/$time_curr/hpc_stack_compute${i}.tf.1
      if [ $CLOUD_STR = 'aws' ]; then
        mv $STACKDIR/hpc_stack_compute${i}_eni.tf $WDIR/.trashbin/$time_curr/hpc_stack_compute${i}_eni.tf.1
      fi
    done
    cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
    getstate
    if [ $? -ne 0 ]; then
      echo -e "[ FATAL: ] Operation failed. Please check the ${logfile} for details."
      exit
    fi
    echo -e "[ -INFO- ] AFTER OPERATION:"
    graph
    echo -e "[ STEP 2 ] Remote executing now ... " && rexec_connect 1 && rexec_clear 2
    echo -e "[ -DONE- ] ALL compute nodes have been deleted."
  elif [ -n "$2" ]; then
    expr $2 + 0 &>/dev/null
    if [ $? -ne 0 ]; then
      echo -e "[ FATAL: ] You need to specify how many nodes to be deleted. \nExit now.\n"
      exit
    else 
      delete_nodes=$2
    fi
    if [ $((delete_nodes)) -ge $((COMPUTE_NODE_NUM)) ]; then
      echo -e "[ -INFO- ] You specified to delete $delete_nodes from $COMPUTE_NODE_NUM compute node(s).\n[ -INFO- ] Will delete all the nodes."
      delete_nodes=$COMPUTE_NODE_NUM
    fi
    #confirmation
    time_curr=`date "+%Y-%m-%d-%H:%M:%S"`
    mkdir -p $WDIR/.trashbin/$time_curr
    echo -e "[ -INFO- ] Node $((COMPUTE_NODE_NUM-delete_nodes+1)) to $COMPUTE_NODE_NUM will be deleted.\n[ STEP 1 ] Deleting the specified node(s) ..."
    for i in $(seq $((COMPUTE_NODE_NUM-delete_nodes+1)) $COMPUTE_NODE_NUM)
    do
      mv $STACKDIR/hpc_stack_compute${i}.tf $WDIR/.trashbin/$time_curr/hpc_stack_compute${i}.tf.1
      if [ $CLOUD_STR = 'aws' ]; then
        mv $STACKDIR/hpc_stack_compute${i}_eni.tf $WDIR/.trashbin/$time_curr/hpc_stack_compute${i}_eni.tf.1
      fi
      #rexec_modify_hostfile compute${i}
    done
    cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
    getstate
    if [ $? -ne 0 ]; then
      echo -e "[ FATAL: ] Operation failed. Please check the ${logfile} for details."
      exit
    fi
    echo -e "[ STEP 2 ] Remote executing now ... " && rexec_connect 1 && rexec_clear 2 && rexec_all 2
    echo -e "[ -DONE- ] Nodes $((COMPUTE_NODE_NUM-delete_nodes+1)) to $COMPUTE_NODE_NUM have been deleted. This operation will take effects in 3 minutes."
    echo -e "[ -INFO- ] AFTER OPERATION:"
    graph
  fi
  exit
fi

if [[ -n $1 && $1 = "resume" ]]; then
  envcheck
  #confirmation
  folder_name=`ls -lt $WDIR/.trashbin/ | head -n2 | tail -n1 | awk '{print $9}'`
  if [ $folder_name = '' ]; then
    echo -e "[ FATAL: ] No recently deleted resources found, nothing resumed. Exit now.\n"
    exit
  fi
  mv $WDIR/.trashbin/$folder_name/* $STACKDIR
  cd $STACKDIR
  for file in `ls *.1`
  do
    mv $file `echo $file | sed 's/.tf.1/.tf/g'`
  done
  echo -e "[WARNING:] This operation may fail. \n[ STEP 1 ] Trying to resume your resources now ..."
  cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
  getstate
  if [ $? -ne 0 ]; then
    echo -e "[ FATAL: ] Operation failed. Please check the ${logfile} for details."
    exit
  fi
  echo -e "[ STEP 2 ] Remote executing now ... "
  if [ $CLOUD_STR = 'aws' ]; then
    for i in $( seq 1 60)
    do
	    sleep 1
    done
  fi
  rexec_connect 7 && rexec_all 8
  echo -e "[ -DONE- ] Resume finished. This operation will take effects in 7 minutes."
  echo -e "[ -INFO- ] AFTER OPERATION:"
  graph
fi

if [[ -n $1 && $1 = "addc" ]]; then
  envcheck
  if [ ! -n "$2" ]; then
    echo -e "[ FATAL: ] You need to specify how many compute nodes to be added to this cluster.\nExit now.\n"
    exit
  else
    expr $2 + 0 &>/dev/null
    if [ $? -ne 0 ]; then
      echo -e "[ FATAL: ] You need to specify how many nodes to be added (UP to 10). \nExit now.\n"
      exit
    else
      add_nodes=$2
    fi
    if [ $((add_nodes)) -gt $((10)) ]; then
      echo -e "[ FATAL: ] Please specify a number less than or equal to 10.\nExit now.\n"
      exit
    fi
    if [ ! -f $STACKDIR/compute_template ]; then
      echo -e "[ FATAL: ] NO compute node template file found. Please run 'init' first.\nExit now.\n"
      exit
    fi
    #confirmation
    echo -e "[ -INFO- ] BEFORE OPERATION:"
    graph 
    START_NUM=$((COMPUTE_NODE_NUM+1))
    END_NUM=$((COMPUTE_NODE_NUM+add_nodes))
    echo -e "[ -INFO- ] $2 Compute Nodes will be added to this cluster.\n[ STEP 1 ] Adding the compute nodes now ..."
    for i in $(seq $START_NUM $END_NUM )
    do
      cp $STACKDIR/compute_template $STACKDIR/hpc_stack_compute${i}.tf
      sed -i "s@compute1@compute${i}@g" $STACKDIR/hpc_stack_compute${i}.tf
      if [ $CLOUD_STR = 'aws' ]; then
        cp $STACKDIR/compute_eni_template $STACKDIR/hpc_stack_compute${i}_eni.tf
        sed -i "s@compute1@compute${i}@g" $STACKDIR/hpc_stack_compute${i}_eni.tf
      fi
    done
    cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
    getstate
    if [ $? -ne 0 ]; then
      echo -e "[ FATAL: ] Operation failed. Please check the ${logfile} for details."
      exit
    fi
    echo -e "[ STEP 2 ] Remote executing now ... " && rexec_connect 7 && rexec_all 8
    echo -e "[ -DONE- ] $2 Compute nodes have been added to this cluster. This operation will take effect in 7 minutes.\n"
    echo -e "[ -INFO- ] AFTER OPERATION:"
    graph 
    exit
  fi
fi

if [[ -n $1 && $1 = "reconfc" ]]; then
  envcheck
  if [ ! -n "$2" ]; then
    echo -e "[ FATAL: ] You need to specify the new configuration of compute nodes.\nExit now.\n"
    exit
  else
    if [[ $CLOUD_STR = 'tencentcloud' || $CLOUD_STR = 'alicloud' ]]; then
      cat $STACKDIR/hpc_stack_base.tf | grep ${CLOUD_STR}_instance_types | grep -w $2 >> /dev/null
    elif [ $CLOUD_STR = 'aws' ]; then
      cat $STACKDIR/hpc_stack_base.tf | grep ${CLOUD_STR}_ec2_instance_types | grep -w $2 >> /dev/null
    fi
    if [ $? -ne 0 ]; then
      echo -e "[ FATAL: ] The configuration is invalid. Nothing changed. Exit now.\n"
      exit
    else
      current_config=`cat $STACKDIR/compute_template | grep instance_type | awk '{print $3}' | awk -F"." '{print $3}'`
      if [ $CLOUD_STR = 'aws' ]; then
        cat $STACKDIR/compute_template | grep "cpu_threads_per_core = 1" >> /dev/null 2>&1
        if [ $? -eq 0 ]; then
          current_ht=htoff
        else
          current_ht=hton
        fi
        current_core_count_line=`cat $STACKDIR/compute_template | grep cpu_core_count`
      fi
      if [[ $CLOUD_STR = 'alicloud' || $CLOUD_STR = 'tencentcloud' ]]; then
        if [ $2 = $current_config ]; then
          echo -e "[ -INFO- ] The new configuration is the same as previous configuration. Nothing changed. Exit now.\n"
          exit
        fi
      elif [ $CLOUD_STR = 'aws' ]; then
        if [[ -n $3 && $3 != 'hton' && $3 != 'htoff' ]]; then
          echo -e "[ FATAL: ] Invalid parameters. The last parameter must be hton/htoff. Or you can leave it none. Exit now.\n"
          exit
        fi
        if [[ $2 = $current_config && -n $3 && $3 = $current_ht ]]; then
          echo -e "[ -INFO- ] The new configuration is the same as previous configuration. Nothing changed. Exit now.\n"
          exit
        fi
        if [[ $2 = $current_config && ! -n $3 ]]; then
          echo -e "[ -INFO- ] The new configuration is the same as previous configuration. Nothing changed. Exit now.\n"
          exit
        fi
      fi
      #confirmation
      echo -e "[ -INFO- ] BEFORE OPERATION:"
      graph
      echo -e "[ STEP 1 ] Reconfiguring the compute nodes to $2 now ... "
      if [ $CLOUD_STR = 'aws' ]; then
          number_of_vcpu=`echo $2 | awk -F"c" '{print $1}' | sed 's/[a-z]//g'`
          new_core_count_num=$((number_of_vcpu/2))
          new_core_count_line="cpu_core_count = "$new_core_count_num
      fi
      for i in $(seq 1 $COMPUTE_NODE_NUM)
      do
        sed -i "s@$current_config@$2@g" $STACKDIR/hpc_stack_compute${i}.tf
        if [ $CLOUD_STR = 'aws' ]; then
          if [[ -n $3 && $3 = 'hton' ]]; then
            WAIT_PERIOD='long'
            sed -i "s@cpu_threads_per_core = 1@cpu_threads_per_core = 2@g" $STACKDIR/hpc_stack_compute${i}.tf
          elif [[ -n $3 && $3 = 'htoff' ]]; then
            WAIT_PERIOD='long'
            sed -i "s@cpu_threads_per_core = 2@cpu_threads_per_core = 1@g" $STACKDIR/hpc_stack_compute${i}.tf
          fi
          sed -i "s@$current_core_count_line@$new_core_count_line@g" $STACKDIR/hpc_stack_compute${i}.tf
        fi
      done
      sed -i "s@$current_config@$2@g" $STACKDIR/compute_template
      if [ $CLOUD_STR = 'aws' ]; then
        if [[ -n $3 && $3 = 'hton' ]]; then
          sed -i "s@cpu_threads_per_core = 1@cpu_threads_per_core = 2@g" $STACKDIR/compute_template 
        elif [[ -n $3 && $3 = 'htoff' ]]; then
          sed -i "s@cpu_threads_per_core = 2@cpu_threads_per_core = 1@g" $STACKDIR/compute_template
        fi
        sed -i "s@$current_core_count_line@$new_core_count_line@g" $STACKDIR/compute_template
      fi
    fi
    cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
    if [ $CLOUD_STR = 'aws' ]; then
      if [[ -n $3 && $3 = 'htoff' ]]; then
        echo -e "[ -INFO- ] Turning off hyperthreading, this may take minutes ... "
        WAIT_PERIOD='long'
        for i in $( seq 1 5)
        do
	        sleep 1
        done
        cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
      elif [[ ! -n $3 && $current_ht = 'htoff' ]]; then
        echo -e "[ -INFO- ] Turning off hyperthreading, this may take minutes ... "
        WAIT_PERIOD='long'
        for i in $( seq 1 5)
        do
	        sleep 1
        done
        cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
      fi
    fi
    if [ $? -ne 0 ]; then
      echo -e "[ FATAL: ] Operation failed. Please check the ${logfile} for details."
      for i in $(seq 1 $COMPUTE_NODE_NUM)
      do
        sed -i "s@$2@$current_config@g" $STACKDIR/hpc_stack_compute${i}.tf
        if [[ -z $WAIT_PERIOD && $3 = 'hton' ]]; then
          sed -i "s@cpu_threads_per_core = 2@cpu_threads_per_core = 1@g" $STACKDIR/hpc_stack_compute${i}.tf
          sed -i "s@$new_core_count_line@$current_core_count_line@g" $STACKDIR/hpc_stack_compute${i}.tf
        elif [[ -z $WAIT_PERIOD && $3 = 'htoff' ]]; then
          sed -i "s@cpu_threads_per_core = 1@cpu_threads_per_core = 2@g" $STACKDIR/hpc_stack_compute${i}.tf
          sed -i "s@$new_core_count_line@$current_core_count_line@g" $STACKDIR/hpc_stack_compute${i}.tf
        fi  
      done
      sed -i "s@$2@$current_config@g" $STACKDIR/compute_template 
      if [[ -z $WAIT_PERIOD && $3 = 'hton' ]]; then
        sed -i "s@cpu_threads_per_core = 2@cpu_threads_per_core = 1@g" $STACKDIR/compute_template
        sed -i "s@$new_core_count_line@$current_core_count_line@g" $STACKDIR/compute_template
      elif [[ -z $WAIT_PERIOD && $3 = 'htoff' ]]; then
        sed -i "s@cpu_threads_per_core = 1@cpu_threads_per_core = 2@g" $STACKDIR/compute_template
        sed -i "s@$new_core_count_line@$current_core_count_line@g" $STACKDIR/compute_template
      fi  
      exit
    fi
    getstate
    if [ -z $WAIT_PERIOD ]; then
      echo -e "[ STEP 2 ] Remote executing now ... " && rexec_connect 7 && rexec_all 8
    else
      echo -e "[ STEP 2 ] Remote executing now ... " && rexec_connect 2 && rexec_all 3
    fi
    echo -e "[ -DONE- ] The Compute Node(s) has been reconfigured and reboot. This operation will take effect in 3 minutes."
    echo -e "[ -INFO- ] AFTER OPERATION:"
    graph 
    exit
  fi
fi 
    
if [[ -n $1 && $1 = "shutdownc" ]]; then
  envcheck
  if [ ! -n "$2" ]; then
    echo -e "[ FATAL: ] You need to specify to shutdown all or some node(s).\nExit now.\n"
    exit
  fi
  if [[ -n "$2" && $2 = 'all' ]]; then
    #confirmation
    echo -e "[ STEP 1 ] Shutting down ALL the compute nodes now ..."
    for i in $(seq 1 $COMPUTE_NODE_NUM )
    do
      if [ $CLOUD_STR = "tencentcloud" ]; then
        sed -i '0,/true/{s/true/false/}' $STACKDIR/hpc_stack_compute${i}.tf
      elif [ $CLOUD_STR = "alicloud" ]; then
        sed -i 's/Running/Stopped/g' $STACKDIR/hpc_stack_compute${i}.tf
      elif [ $CLOUD_STR = "aws" ]; then
        root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
        master_address=`cat $STACKDIR/currentstate | head -n1`
        sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "ssh compute${i} 'shutdown now' "
        line_num=`sed = $STACKDIR/terraform.tfstate | sed 'N;s/\n/\t/' | sed -n "/\"name\": \"compute$i\"/,+60p" | grep -w instance_state | awk '{print $1}'`
        sed -i "${line_num} s/running/stopped/g" $STACKDIR/terraform.tfstate 
        #Although it is strongly recommended not to modify tfstate file, but I have to do it, because AWS didn't provide state management feature for ec2 instances. 
      fi
    done
    if [[ $CLOUD_STR = "tencentcloud" || $CLOUD_STR = "alicloud" ]]; then
      cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
    fi
    getstate
    if [ $? -ne 0 ]; then
      echo -e "[ FATAL: ] Operation failed. Please check the ${logfile} for details."
      exit
    fi
    echo -e "[ STEP 2 ] Remote executing now ... " && rexec_connect 1
    echo -e "[ -DONE- ] All the compute nodes are turned off. You can run 'hpcopr allupc' to turn them on.\n[ -INFO- ] AFTER OPERATION:"
    graph 
    exit
  elif [ -n "$2" ]; then
    expr $2 + 0 &>/dev/null
    if [ $? -ne 0 ]; then
      echo -e "[ FATAL: ] You need to specify to shutdown all or some node(s). \nExit now.\n"
      exit
    else 
      down_nodes=$2
    fi
    if [ $((down_nodes)) -ge $((COMPUTE_NODE_NUM)) ]; then
      echo -e "[ -INFO- ] You specified to shutdown $down_nodes from $COMPUTE_NODE_NUM compute node(s). Will shutdown all the nodes."
      down_nodes=$COMPUTE_NODE_NUM
    fi
    #confirmation
    echo -e "[ -INFO- ] Node $((COMPUTE_NODE_NUM-down_nodes+1)) to $COMPUTE_NODE_NUM will be shutdown.\n[ STEP 1 ] Shutting down the specified nodes ..."
    for i in $(seq $((COMPUTE_NODE_NUM-down_nodes+1)) $COMPUTE_NODE_NUM)
    do
      if [ $CLOUD_STR = "tencentcloud" ]; then
        sed -i '0,/true/{s/true/false/}' $STACKDIR/hpc_stack_compute${i}.tf
      elif [ $CLOUD_STR = "alicloud" ]; then
        sed -i 's/Running/Stopped/g' $STACKDIR/hpc_stack_compute${i}.tf
      elif [ $CLOUD_STR = "aws" ]; then
        root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
        master_address=`cat $STACKDIR/currentstate | head -n1`
        sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "ssh compute${i} 'shutdown now' "
        line_num=`sed = $STACKDIR/terraform.tfstate | sed 'N;s/\n/\t/' | sed -n "/\"name\": \"compute$i\"/,+60p" | grep -w instance_state | awk '{print $1}'`
        sed -i "${line_num} s/running/stopped/g" $STACKDIR/terraform.tfstate 
        #Although it is strongly recommended not to modify tfstate file, but I have to do it, because AWS didn't provide state management feature for ec2 instances. 
      fi
    done
    if [[ $CLOUD_STR = "tencentcloud" || $CLOUD_STR = "alicloud" ]]; then
      cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
    fi
    getstate
    if [ $? -ne 0 ]; then
      echo -e "[ FATAL: ] Operation failed. Please check the ${logfile} for details."
      exit
    fi
    echo -e "[ STEP 2 ] Remote executing now ... " && rexec_connect 1
    echo -e "[ -DONE- ] Node $((COMPUTE_NODE_NUM-down_nodes+1)) to $COMPUTE_NODE_NUM has been shutdown."
    echo -e "[ -INFO- ] AFTER OPERATION:"
    graph 
  fi
  exit  
fi

if [[ -n $1 && $1 = "allupc" ]]; then
  envcheck
  #confirmation
  if [[ $CLOUD_STR = 'alicloud' || $CLOUD_STR = 'tencentcloud' ]]; then
    echo -e "[ STEP 1 ] Turning all the compute node(s) on ..."
    for i in $(seq 1 $COMPUTE_NODE_NUM )
    do
      if [ $CLOUD_STR = "alicloud" ]; then
        sed -i 's/Stopped/Running/g' $STACKDIR/hpc_stack_compute${i}.tf
      elif [ $CLOUD_STR = "tencentcloud" ]; then
        sed -i '0,/false/{s/false/true/}' $STACKDIR/hpc_stack_compute${i}.tf
      fi
    done
    cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
  elif [ $CLOUD_STR = 'aws' ]; then
    current_inst_type=`cat $STACKDIR/compute_template | grep instance_type | awk -F"." '{print $3}'`
    echo -e "[ STEP 1 ] Turning all the compute node(s) on ..."
    for i in $(seq 1 $COMPUTE_NODE_NUM )
    do
      node_status=`sed -n '/\"name\": \"compute'$i'\"/,+60p' $STACKDIR/terraform.tfstate | grep -w instance_state | awk -F"\"" '{print $4}'`
      if [ $node_status != 'running' ]; then
        if [ $current_inst_type != 'a2c4g' ]; then
          sed -i "s/$current_inst_type/a2c4g/g" $STACKDIR/hpc_stack_compute${i}.tf
        else
          sed -i "s/$current_inst_type/a4c8g/g" $STACKDIR/hpc_stack_compute${i}.tf
        fi
      fi
    done
    cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
    for i in $(seq 1 $COMPUTE_NODE_NUM )
    do
      if [ $current_inst_type != 'a2c4g' ]; then
        sed -i "s/a2c4g/$current_inst_type/g" $STACKDIR/hpc_stack_compute${i}.tf
      else
        sed -i "s/a4c8g/$current_inst_type/g" $STACKDIR/hpc_stack_compute${i}.tf
      fi
    done
    cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
  fi
  if [ $? -ne 0 ]; then
      echo -e "[ FATAL: ] Operation failed. Please check the ${logfile} for details."
      exit
  fi
  getstate
  echo -e "[ STEP 2 ] Remote executing now ... " && rexec_connect 2 && rexec_all 3
  echo -e "[ -DONE- ] ALL Compute nodes are up now. This operation will take effects in 3 minutes.\n"
  echo -e "[ -INFO- ] AFTER OPERATION:"
  graph 
  exit
fi

if [[ -n $1 && $1 = "updateconf" ]]; then
  envcheck
  #confirmation
  echo -e "[ -INFO ] You are updating the initialization configuration now ... "
  /bin/cp $CONFDIR/tf_prep.conf $CONFDIR/tf_prep.conf.bkup
  current_config=`cat $STACKDIR/compute_template | grep instance_type | awk '{print $3}' | awk -F"." '{print $3}'`
  current_node_num=`ls -ll $STACKDIR | grep hpc_stack_compute | wc -l`
  prev_compute_inst=`cat $CONFDIR/tf_prep.conf | grep compute_inst`
  new_compute_inst="compute_inst        : "$current_config
  sed -i "s/$prev_compute_inst/$new_compute_inst/g" $CONFDIR/tf_prep.conf
  prev_NODE_NUM=`cat $CONFDIR/tf_prep.conf | grep NODE_NUM`
  new_NODE_NUM="NODE_NUM            : "$current_node_num
  if [ $current_node_num -ne 0 ]; then
    sed -i "s/$prev_NODE_NUM/$new_NODE_NUM/g" $CONFDIR/tf_prep.conf
  else
    echo -e "NODE_NUM unchanged."
  fi
  echo -e "[ -DONE- ] The initialization configuration has been updated."
  exit
fi

if [[ -n $1 && $1 = "destroy" ]]; then
  echo -e "\n********************************CAUTION!********************************\n\nYOU ARE DELETING THE WHOLE CLUSTER - INCLUDING ALL THE NODES AND *DATA*! \nTHIS OPERATION IS UNRECOVERABLE! \n\n********************************CAUTION!********************************\n"
  #confirmation
  if [ $CLOUD_STR = 'aws' ]; then
    bucket_name=`cat $STACKDIR/terraform.tfstate | grep -w \"bucket\" | head -n1 | awk -F"\"" '{print $4}'`
    root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
    master_address=`cat $STACKDIR/currentstate | head -n1`
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "/bin/s3cmd del -rf s3://$bucket_name"
  elif [ $CLOUD_STR = 'tencentcloud' ]; then
    root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
    master_address=`cat $STACKDIR/currentstate | head -n1`
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "/usr/local/bin/coscmd delete -rf /"
  elif [ $CLOUD_STR = 'alicloud' ]; then
    bucket_name=`cat $STACKDIR/terraform.tfstate | grep -w \"bucket\" | head -n1 | awk -F"\"" '{print $4}'`
    root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
    master_address=`cat $STACKDIR/currentstate | head -n1`
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "/usr/local/bin/coscmd delete -rf oss://$bucket_name"
  fi
  cd $STACKDIR && echo "yes" | terraform destroy >> ${logfile_raw} 2>${logfile} && rm -rf $STACKDIR/*.tf && rm -rf $STACKDIR/currentstate && rm -rf $STACKDIR/compute_template
  rm -rf $VAULTDIR/root_passwds.txt
  rm -rf $VAULTDIR/bucket_secrets.txt
  rm -rf $VAULTDIR/db_passwds.txt
  rm -rf $VAULTDIR/_CLUSTER_SUMMARY.txt
  rm -rf $STACKDIR/currentstate
  rm -rf $STACKDIR/compute_template
  if [ $CLOUD_STR = 'aws' ]; then
    rm -rf $STACKDIR/compute_eni_template
  fi
  mv $CONFDIR/tf_prep.conf $CONFDIR/tf_prep.conf.destroyed
  echo -e "[ -DONE- ] The whole cluster has been destroyed. You can run 'init' command to rebuild it, but all the data has been erased permenantly."
  exit
fi

if [[ -n $1 && $1 = "conf" ]]; then
  if [[ -f $CONFDIR/tf_prep.conf || -f $WDIR/tf_prep.conf ]]; then
    echo -e "[ -INFO- ] The configuration file already exists. Exit now.\n"
    exit
  fi
  if [ ! -d $CONFDIR ]; then
    mkdir -p $CONFDIR
  fi
  echo -e "[ -INFO- ] Creating the configuration file template now ... "
  if [ $CLOUD_STR = "alicloud" ]; then
    wget ${URL_ROOT_ALI}tf_prep.conf -O $CONFDIR/tf_prep.conf -q
  elif [ $CLOUD_STR = "tencentcloud" ]; then
    wget ${URL_ROOT_QCLOUD}tf_prep.conf -O $CONFDIR/tf_prep.conf -q
  elif [ $CLOUD_STR = "aws" ]; then
    wget ${URL_ROOT_AWS}tf_prep.conf -O $CONFDIR/tf_prep.conf -q
  fi
  echo -e "[ -DONE- ] The configuration template has been downloaded to the conf folder.\n"
fi

if [[ -n $1 && $1 = "monthpay" ]]; then
  envcheck
  if [ $CLOUD_STR = 'aws' ]; then
    echo -e "[ FATAL: ] This opeartion is not supported by your current cluster. Exit now.\n"
    exit
  elif [ $CLOUD_STR = 'alicloud' ]; then
    echo -e "[WARNING:] You are changing the charge type of the cluster. This operation is not reversable."
    echo -e "[ -INFO- ] You have $COMPUTE_NODE_NUM compute node(s)."
    cat $STACKDIR/compute_template | grep "instance_charge_type = PrePaid" >> /dev/null 2>&1
    if [ $? -eq 0 ]; then
      echo -e "[ -INFO- ] The cluster is already in Monthly-payment mode. Nothing changed. Exit now.\n"
      exit
    else
      sed -i '/user_data/i\  instance_charge_type = PrePaid' $STACKDIR/hpc_stack_master.tf
      sed -i '/user_data/i\  period_unit = Month' $STACKDIR/hpc_stack_master.tf
      sed -i '/user_data/i\  period = 1' $STACKDIR/hpc_stack_master.tf
      sed -i '/user_data/i\  instance_charge_type = PrePaid' $STACKDIR/hpc_stack_database.tf
      sed -i '/user_data/i\  period_unit = Month' $STACKDIR/hpc_stack_database.tf
      sed -i '/user_data/i\  period = 1' $STACKDIR/hpc_stack_database.tf
      sed -i '/user_data/i\  instance_charge_type = PrePaid' $STACKDIR/hpc_stack_natgw.tf
      sed -i '/user_data/i\  period_unit = Month' $STACKDIR/hpc_stack_natgw.tf
      sed -i '/user_data/i\  period = 1' $STACKDIR/hpc_stack_natgw.tf
      for i in $(seq 1 $COMPUTE_NODE_NUM)
      do
        sed -i '/user_data/i\  instance_charge_type = PrePaid' $STACKDIR/hpc_stack_compute${i}.tf
        sed -i '/user_data/i\  period_unit = Month' $STACKDIR/hpc_stack_compute${i}tf
        sed -i '/user_data/i\  period = 1' $STACKDIR/hpc_stack_compute${i}.tf
      done
      if [ -f $STACKDIR/compute_template ]; then
        sed -i '/user_data/i\  instance_charge_type = PrePaid' $STACKDIR/compute_template
        sed -i '/user_data/i\  period_unit = Month' $STACKDIR/compute_template
        sed -i '/user_data/i\  period = 1' $STACKDIR/compute_template
      fi
    fi
  elif [ $CLOUD_STR = 'tencentcloud' ]; then
    echo -e "[WARNING:] You are changing the charge type of the cluster. This operation is not reversable."
    echo -e "[ -INFO- ] You have $COMPUTE_NODE_NUM compute node(s)."
    cat $STACKDIR/compute_template | grep "instance_charge_type = PREPAID" >> /dev/null 2>&1
    if [ $? -eq 0 ]; then
      echo -e "[ -INFO- ] The cluster is already in Monthly-payment mode. Nothing changed. Exit now.\n"
      exit
    else
      sed -i '/user_data/i\  instance_charge_type = PREPAID' $STACKDIR/hpc_stack_master.tf
      sed -i '/user_data/i\  instance_charge_type_prepaid_period = 1' $STACKDIR/hpc_stack_master.tf
      sed -i '/user_data/i\  instance_charge_type = PREPAID' $STACKDIR/hpc_stack_database.tf
      sed -i '/user_data/i\  instance_charge_type_prepaid_period = 1' $STACKDIR/hpc_stack_database.tf
      sed -i '/user_data/i\  instance_charge_type = PREPAID' $STACKDIR/hpc_stack_natgw.tf
      sed -i '/user_data/i\  instance_charge_type_prepaid_period = 1' $STACKDIR/hpc_stack_natgw.tf
      for i in $(seq 1 $COMPUTE_NODE_NUM)
      do
        sed -i '/user_data/i\  instance_charge_type = PREPAID' $STACKDIR/hpc_stack_compute${i}.tf
        sed -i '/user_data/i\  instance_charge_type_prepaid_period = 1' $STACKDIR/hpc_stack_compute${i}tf
      done
      if [ -f $STACKDIR/compute_template ]; then
        sed -i '/user_data/i\  instance_charge_type = PREPAID' $STACKDIR/compute_template
        sed -i '/user_data/i\  instance_charge_type_prepaid_period = 1' $STACKDIR/compute_template
      fi
    fi
  fi
  cd $STACKDIR && echo "yes" | terraform destroy >> ${logfile_raw} 2>${logfile}
  echo -e "[ -DONE- ] The cluster has been changed to monthly-pay mode."
fi

if [[ -n $1 && $1 = "init" && $CLOUD_STR = "alicloud" ]]; then
  if [[ -f $STACKDIR/currentstate || -f $STACKDIR/compute_template ]]; then
    echo -e "[ FATAL: ] It seems the cluster is already in place. Please empty your stack folder and retry. Exit now.\n"
    exit
  fi
  echo -e "[ START: ] Start initializing the cluster ... "
  ########## To make sure all the subdirs exist ######
  if [ ! -d $STACKDIR ]; then
    mkdir -p $STACKDIR
  fi
  if [ ! -d $VAULTDIR ]; then
    mkdir -p $VAULTDIR
  fi
  if [ ! -d $CONFDIR ]; then
    mkdir -p $CONFDIR
    if [ -f tf_prep.conf ]; then
      mv tf_prep.conf $CONFDIR
    fi
  fi
  if [ ! -d $LOGDIR ]; then
    mkdir -p $LOGDIR
  fi
  if [ ! -f $CONFDIR/tf_prep.conf ]; then
    echo -e "[ -INFO- ] IMPORTANT: No configure file found. Using the default configure file to initialize this cluster.\n[ -INFO- ] IMPORTANT: By default, the initialization process includes building OpenMPI-4.1.2, OpenMPI-3.1.6, and MPICH-3.2.1. \n[ -INFO- ] By default, NO HPC software will be built into the cluster. Please run 'hpcmgr install' command to install the software you need."
    wget ${URL_ROOT_ALI}tf_prep.conf -O $CONFDIR/tf_prep.conf -q
  fi
  #confirmation
  cd $STACKDIR
  echo -e "[ STEP 1 ] Creating input files now..."  
  rm -rf hpc_stack_*
  rm -rf compute_template
  wget ${URL_ROOT_ALI}NAS_Zones_ALI.txt -O NAS_Zones_ALI.txt -q
  wget ${URL_ROOT_ALI}hpc_stackv2.base -O .hpc_stack.base -q
  wget ${URL_ROOT_ALI}hpc_stackv2.master -O .hpc_stack.master -q
  wget ${URL_ROOT_ALI}hpc_stackv2.compute -O .hpc_stack.compute -q
  wget ${URL_ROOT_ALI}hpc_stackv2.database -O .hpc_stack.database -q
  wget ${URL_ROOT_ALI}hpc_stackv2.natgw -O .hpc_stack.natgw -q

  if [[ ! -f .hpc_stack.base || ! -f .hpc_stack.master || ! -f .hpc_stack.compute || ! -f NAS_Zones_ALI.txt || ! -f .hpc_stack.natgw || ! -f .hpc_stack.database ]]; then
    echo -e "[ FATAL: ] Input files download failed. Exit now.\n"
    exit
  fi
  
  access_key_id=`cat $VAULTDIR/.secrets.txt | head -n1 | awk '{print $1}'`
  access_key_secret=`cat $VAULTDIR/.secrets.txt | head -n2 | tail -n1 | awk '{print $1}'`
  CONF_FILE="$CONFDIR/tf_prep.conf"
  CLUSTER_ID=`cat $CONF_FILE | grep CLUSTER_ID | cut -c 23-28`
  REGION_ID=`cat $CONF_FILE | grep REGION_ID | cut -c 23-`
  ZONE_ID=`cat $CONF_FILE | grep ZONE_ID | cut -c 23-`
  NODE_NUM=`cat $CONF_FILE | grep NODE_NUM | cut -c 23-`
  HPC_USER_NUM_tmp=`cat $CONF_FILE | grep HPC_USER_NUM | cut -c 23-`
  master_init_param=`cat $CONF_FILE | grep master_init_param | cut -c 23-`
  compute_init_param=`cat $CONF_FILE | grep compute_init_param | cut -c 23-`
  master_passwd=`cat $CONF_FILE | grep master_passwd | cut -c 23-`
  compute_passwd=`cat $CONF_FILE | grep compute_passwd | cut -c 23-`
  master_inst=`cat $CONF_FILE | grep master_inst | cut -c 23-`
  master_bandwidth=`cat $CONF_FILE | grep master_bandwidth | cut -c 23-`
  compute_inst=`cat $CONF_FILE | grep compute_inst | cut -c 23-`
  compute_bandwidth=`cat $CONF_FILE | grep compute_bandwidth | cut -c 23-`
  database_inst=`cat $CONF_FILE | grep database_inst | cut -c 23-`
  database_bandwidth=`cat $CONF_FILE | grep database_bandwidth | cut -c 23-`
  os_image=`cat $CONF_FILE | grep os_image | cut -c 23-`
  NAS_ZONE=`cat NAS_Zones_ALI.txt | grep ${REGION_ID}`
  stopped_node_from=`cat $CONF_FILE | grep stopped_node_from | cut -c 23-`
  
  if [ $((stopped_node_from)) -gt $((NODE_NUM)) ]; then
    echo -e "[ -INFO- ] All nodes will be in the state of Running."
  fi

#AUTOMATICALLY GENERATE DB passwords
  if [ ! -f $VAULTDIR/db_passwds.txt ]; then
    database_root_passwd=`openssl rand -base64 8`
    database_acct_passwd=`openssl rand -base64 8`
    echo -e "$database_root_passwd\n$database_acct_passwd" > $VAULTDIR/db_passwds.txt
  else
    database_root_passwd=`cat $VAULTDIR/db_passwds.txt | head -n1`
    database_acct_passwd=`cat $VAULTDIR/db_passwds.txt | tail -n1`
  fi

  if [ $HPC_USER_NUM_tmp = "*default*" ]; then
    HPC_USER_NUM=3
  elif [ ! -n $HPC_USER_NUM_tmp ]; then
    echo -e "[ -INFO- ] HPC_USER_NUM is not a number or '*default*', set to default value 3."
    HPC_USER_NUM=3
  else
    HPC_USER_NUM=$HPC_USER_NUM_tmp
  fi

  if [ $master_passwd = "*AUTOGEN*" ]; then
    master_passwd=`openssl rand -base64 8`
  fi
  if [ $compute_passwd = "*AUTOGEN*" ]; then
    compute_passwd=`openssl rand -base64 8`
  fi

  if [[ -n "$2" && $2 = 'newucid' ]]; then
    if [[ -n "$3" && $3 = "agpswd" ]]; then
      master_passwd=`openssl rand -base64 8`
      compute_passwd=`openssl rand -base64 8`
    fi
    if [ -f $VAULTDIR/UCID_LATEST.txt ]; then
      echo -e "[ -INFO- ] Creating a new Unique Cluster ID now..."
      prev_time_stamp=`cat $VAULTDIR/UCID_LATEST.txt | grep DATE: | cut -c 7-`
      prev_ucid=`cat $VAULTDIR/UCID_LATEST.txt | grep ID: | cut -c 5-`
      echo -e "$prev_time_stamp\t$prev_ucid" >> $VAULTDIR/UCID_ARCHIVE.txt
      time_curr=`date "+%Y-%m-%d-%H:%M:%S"`
      rand_str=`openssl rand -hex 4`
      ucid=$CLUSTER_ID$rand_str
      echo -e "[ -INFO- ] #NEW Unique Cluster ID created.\nDATE: $time_curr\nID: $ucid" > $VAULTDIR/UCID_LATEST.txt
    else
      echo -e "[ -INFO- ] UCID_LATEST.txt does not exist. Creating a Unique Cluster ID now..."
      time_curr=`date "+%Y-%m-%d-%H:%M:%S"`
      rand_str=`openssl rand -hex 4`
      ucid=$CLUSTER_ID$rand_str
      echo -e "[ -INFO- ] #NEW Unique Cluster ID created.\nDATE: $time_curr\nID: $ucid" > $VAULTDIR/UCID_LATEST.txt
    fi
  fi
 
  if [ ! -f $VAULTDIR/UCID_LATEST.txt ]; then
    echo -e "[ -INFO- ] UCID_LATEST.txt does not exist. Creating a Unique Cluster ID now..."
    time_curr=`date "+%Y-%m-%d-%H:%M:%S"`
    rand_str=`openssl rand -hex 4`
    ucid=$CLUSTER_ID$rand_str
    echo -e "[ -INFO- ] #NEW Unique Cluster ID created.\nDATE: $time_curr\nID: $ucid" > $VAULTDIR/UCID_LATEST.txt
  fi
  echo -e "$master_passwd\n$compute_passwd" > $VAULTDIR/root_passwds.txt

  ucid_str=`cat $VAULTDIR/UCID_LATEST.txt | grep ID: | cut -c 5-17`  
  echo -e "[ STEP 2 ] CLUSTER CONFIGURATION:\n\tCluster ID: $ucid_str\n\tRegion: $REGION_ID\n\tAvalability Zone: $ZONE_ID\n\t# of Nodes: $NODE_NUM\n\t# of Users: $HPC_USER_NUM\n\tMaster Node Root Password: (***sensitive***)\n\tCompute Node(s) Root Password: (***sensitive***)\n\tMaster Node Instance: $master_inst\n\tCompute Node(s) Instance: $compute_inst\n\tDatabase Node Instance: $database_inst\n\tOS Image: $os_image\n[ -INFO- ] Building you cluster now, this may take seconds ..."

# modify base file
  sed -i "s@BLANK_ACCESS_KEY_ID@$access_key_id@g" .hpc_stack.base
  sed -i "s@BLANK_SECRET_KEY@$access_key_secret@g" .hpc_stack.base
  sed -i "s@BUCKET_ID@$ucid_str@g" .hpc_stack.base
  sed -i "s@BUCKET_ACCESS_POLICY@$ucid_str@g" .hpc_stack.base
  sed -i "s@BUCKET_USER_ID@$ucid_str@g" .hpc_stack.base
  sed -i "s@SECURITY_GROUP_PUBLIC@${ucid_str}public@g" .hpc_stack.base
  sed -i "s@SECURITY_GROUP_INTRA@${ucid_str}intra@g" .hpc_stack.base
  sed -i "s@RG_NAME@${ucid_str}rgname@g" .hpc_stack.base  
  sed -i "s@RG_DISPLAY_NAME@${ucid_str}rgdispname@g" .hpc_stack.base
  sed -i "s@SECURITY_GROUP_MYSQL@${ucid_str}mysql@g" .hpc_stack.base
  sed -i "s@NAS_ACCESS_GROUP_@${ucid_str}nag@g" .hpc_stack.base
  sed -i "s@DEFAULT_REGION_ID@$REGION_ID@g" .hpc_stack.base
  sed -i 's/DEFAULT_ZONE_ID/'$ZONE_ID'/g' .hpc_stack.base
  sed -i 's/DEFAULT_NAS_ZONE/'$NAS_ZONE'/g' .hpc_stack.base
  sed -i 's/DEFAULT_NODE_NUM/'$NODE_NUM'/g' .hpc_stack.base
  sed -i 's/DEFAULT_USER_NUM/'$HPC_USER_NUM'/g' .hpc_stack.base
  sed -i "s@DEFAULT_MASTERINI@$master_init_param@" .hpc_stack.base
  sed -i "s@DEFAULT_COMPUTEINI@$compute_init_param@" .hpc_stack.base
  sed -i "s@DEFAULT_MASTER_PASSWD@$master_passwd@g" .hpc_stack.base
  sed -i "s@DEFAULT_COMPUTE_PASSWD@$compute_passwd@g" .hpc_stack.base
  sed -i "s@DEFAULT_DB_ROOT_PASSWD@$database_root_passwd@g" .hpc_stack.base
  sed -i "s@DEFAULT_DB_ACCT_PASSWD@$database_acct_passwd@g" .hpc_stack.base

# modify master and compute file
  sed -i 's/DEFAULT_ZONE_ID/'$ZONE_ID'/g' .hpc_stack.master
  sed -i 's/MASTER_INST/'"$master_inst"'/g' .hpc_stack.master
  sed -i 's/CLOUD_FLAG/'$CLOUD_FLAG'/g' .hpc_stack.master
  sed -i 's/MASTER_BANDWIDTH/'"$master_bandwidth"'/g' .hpc_stack.master
  sed -i 's/OS_IMAGE/'$os_image'/g' .hpc_stack.master
  sed -i 's/DEFAULT_ZONE_ID/'$ZONE_ID'/g' .hpc_stack.compute
  sed -i 's/COMPUTE_INST/'$compute_inst'/g' .hpc_stack.compute
  sed -i 's/COMPUTE_BANDWIDTH/'$compute_bandwidth'/g' .hpc_stack.compute
  sed -i 's/OS_IMAGE/'$os_image'/g' .hpc_stack.compute
  sed -i 's/DEFAULT_ZONE_ID/'$ZONE_ID'/g' .hpc_stack.database
  sed -i 's/DB_INST/'$database_inst'/g' .hpc_stack.database
  sed -i 's/DATABASE_BANDWIDTH/'$database_bandwidth'/g' .hpc_stack.database
#  sed -i 's/OS_IMAGE/centos7/g' .hpc_stack.database
  sed -i 's/DEFAULT_ZONE_ID/'$ZONE_ID'/g' .hpc_stack.natgw
  sed -i 's/MASTER_BANDWIDTH/'"$master_bandwidth"'/g' .hpc_stack.natgw

  for i in $(seq 1 $NODE_NUM )
  do
    /bin/cp .hpc_stack.compute .hpc_stack.compute${i}
    if [ $((i)) -ge $((stopped_node_from)) ]; then
      sed -i 's/Running/Stopped/g' .hpc_stack.compute${i}
    fi
    sed -i 's/COMPUTE_NODE_N/compute'${i}'/g' .hpc_stack.compute${i}  
  done

  mv .hpc_stack.base hpc_stack_base.tf
  mv .hpc_stack.database hpc_stack_database.tf
  mv .hpc_stack.master hpc_stack_master.tf
  mv .hpc_stack.natgw hpc_stack_natgw.tf
  
  for i in $(seq 1 $NODE_NUM )
  do
    mv .hpc_stack.compute${i} hpc_stack_compute${i}.tf
  done
  rm -rf .hpc_stack*
  rm -rf NAS_Zones*.txt
  terraform init >> ${logfile_raw} 2>${logfile}
  if [ $? -ne 0 ]; then
    echo -e "[WARNING:] Cluster initialization encountered problems. Please check the logfile for details."
    exit
  fi
  cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
  if [ $? -ne 0 ]; then
    echo -e "[WARNING:] Cluster initialization encountered problems. Please check the logfile for details."
    exit
  fi
  /bin/cp hpc_stack_compute1.tf compute_template
  getstate
  if [ -f $STACKDIR/bucket_secrets.txt ]; then
    bucket_name=`cat $STACKDIR/terraform.tfstate | grep -w \"bucket\" | head -n1 | awk -F"\"" '{print $4}'`
    bucket_secret_id=`cat $STACKDIR/bucket_secrets.txt  | grep AccessKeyId | awk -F"\"" '{print $4}'`
    bucket_access_secret=`cat $STACKDIR/bucket_secrets.txt  | grep AccessKeySecret | awk -F"\"" '{print $4}'`
    rm -rf $STACKDIR/bucket_secrets.txt
    echo -e "This is the key pair for you to use the NowNetDisk.\nPowered by Shanghai HPC-Now Co., Ltd\n*** DO NOT share the key pair with any untrusted ones! \nSecret Key ID:\t\t$bucket_secret_id\nAccess Secret Key:\t$bucket_access_secret" > $VAULTDIR/bucket_secrets.txt
  fi
  root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
  master_address=`cat $STACKDIR/currentstate | head -n1`
  echo -e "HPC-NOW CLUSTER SUMMARY\nMaster Node IP: $master_address\nMaster Node Root Password: $root_passwd\n\nNetDisk Address: oss://$bucket_name\nNetDisk Region: $REGION_ID\nNetDisk AccessKey ID: $bucket_secret_id\nNetDisk Secret Key: $bucket_access_secret\n" > $VAULTDIR/_CLUSTER_SUMMARY.txt
  sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "wget ${URL_ROOT_ALI}.ossutilconfig -O /root/.ossutilconfig -q && sed -i 's#BLANK_ACCESS_KEY#$bucket_secret_id#g' /root/.ossutilconfig && sed -i 's#BLANK_SECRET_KEY#$bucket_access_secret#g' /root/.ossutilconfig && sed -i 's#DEFAULT_REGION#$REGION_ID#g' /root/.ossutilconfig && chmod 644 /root/.ossutilconfig"
  echo -e "[ STEP 2 ] Remote executing now ... " 
  for i in $( seq 1 15)
  do
	  sleep 1
  done
  rexec_connect 7 && rexec_all 8
  echo -e "[ -INFO- ] AFTER OPERATION:"
  graph
  echo -e "[ -DONE- ] Congratulations! The cluster is initializing now. This step may take at least 7 minutes. \n[ -INFO- ] After 7 minutes of waiting, you can log in the master node and manage your cluster.\n[ -INFO- ] IMPORTANT: By default, the initialization of the cluster includes building OpenMPI-3.1.6, OpenMPI-4.1.2, and MPICH-3.2.1.\n[ -INFO- ] Building these components needs extra 10 minutes.\n[ -INFO- ] Please check the initialization progress in the /root/cluster_init.log.\n[ -INFO- ] By default, NO HPC software will be built into the cluster. Please run 'hpcmgr install' command to install the software you need.\n"
  exit
fi

if [[ -n $1 && $1 = "init" && $CLOUD_STR = "tencentcloud" ]]; then
  if [[ -f $STACKDIR/currentstate || -f $STACKDIR/compute_template ]]; then
    echo -e "[ FATAL: ] It seems the cluster is already in place. Please empty your stack folder and retry. Exit now.\n"
    exit
  fi
  echo -e "[ START: ] Start initializing the cluster ... "
  ########## To make sure all the subdirs exist ######
  if [ ! -d $STACKDIR ]; then
    mkdir -p $STACKDIR
  fi
  if [ ! -d $VAULTDIR ]; then
    mkdir -p $VAULTDIR
  fi
  if [ ! -d $CONFDIR ]; then
    mkdir -p $CONFDIR
    if [ -f tf_prep.conf ]; then
      mv tf_prep.conf $CONFDIR
    fi
  fi
  if [ ! -d $LOGDIR ]; then
    mkdir -p $LOGDIR
  fi
  if [ ! -f $CONFDIR/tf_prep.conf ]; then
    echo -e "[ -INFO- ] IMPORTANT: No configure file found. Using the default configure file to initialize this cluster.\n[ -INFO- ] IMPORTANT: By default, the initialization process includes building OpenMPI-4.1.2, OpenMPI-3.1.6, and MPICH-3.2.1.\n[ -INFO- ] By default, NO HPC software will be built into the cluster. Please run 'hpcmgr install' command to install the software you need."
    wget ${URL_ROOT_QCLOUD}tf_prep.conf -O $CONFDIR/tf_prep.conf -q
  fi
  #confirmation
  cd $STACKDIR
  echo -e "[ STEP 1 ] Creating input files now..."  
  rm -rf hpc_stack_*
  rm -rf compute_template
  wget ${URL_ROOT_QCLOUD}NAS_Zones_QCloud.txt -O NAS_Zones_QCloud.txt -q
  wget ${URL_ROOT_QCLOUD}hpc_stack_qcloud.base -O .hpc_stack.base -q
  wget ${URL_ROOT_QCLOUD}hpc_stack_qcloud.master -O .hpc_stack.master -q
  wget ${URL_ROOT_QCLOUD}hpc_stack_qcloud.compute -O .hpc_stack.compute -q
  wget ${URL_ROOT_QCLOUD}hpc_stack_qcloud.database -O .hpc_stack.database -q
  wget ${URL_ROOT_QCLOUD}hpc_stack_qcloud.natgw -O .hpc_stack.natgw -q

  if [[ ! -f .hpc_stack.base || ! -f .hpc_stack.master || ! -f .hpc_stack.compute || ! -f NAS_Zones_QCloud.txt || ! -f .hpc_stack.natgw || ! -f .hpc_stack.database ]]; then
    echo -e "[ FATAL: ] Input files download failed. Exit now.\n"
    exit
  fi
  access_key_id=`cat $VAULTDIR/.secrets.txt | head -n1 | awk '{print $1}'`
  access_key_secret=`cat $VAULTDIR/.secrets.txt | head -n2 | tail -n1 | awk '{print $1}'`
  CONF_FILE="$CONFDIR/tf_prep.conf"
  CLUSTER_ID=`cat $CONF_FILE | grep CLUSTER_ID | cut -c 23-28`
  REGION_ID=`cat $CONF_FILE | grep REGION_ID | cut -c 23-`
  ZONE_ID=`cat $CONF_FILE | grep ZONE_ID | cut -c 23-`
  NODE_NUM=`cat $CONF_FILE | grep NODE_NUM | cut -c 23-`
  HPC_USER_NUM_tmp=`cat $CONF_FILE | grep HPC_USER_NUM | cut -c 23-`
  master_init_param=`cat $CONF_FILE | grep master_init_param | cut -c 23-`
  compute_init_param=`cat $CONF_FILE | grep compute_init_param | cut -c 23-`
  master_passwd=`cat $CONF_FILE | grep master_passwd | cut -c 23-`
  compute_passwd=`cat $CONF_FILE | grep compute_passwd | cut -c 23-`
  master_inst=`cat $CONF_FILE | grep master_inst | cut -c 23-`
  master_bandwidth=`cat $CONF_FILE | grep master_bandwidth | cut -c 23-`
  compute_inst=`cat $CONF_FILE | grep compute_inst | cut -c 23-`
  compute_bandwidth=`cat $CONF_FILE | grep compute_bandwidth | cut -c 23-`
  database_inst=`cat $CONF_FILE | grep database_inst | cut -c 23-`
  database_bandwidth=`cat $CONF_FILE | grep database_bandwidth | cut -c 23-`
  os_image=`cat $CONF_FILE | grep os_image | cut -c 23-`
  NAS_ZONE=`cat NAS_Zones_QCloud.txt | grep ${REGION_ID} | tail -n1`
  stopped_node_from=`cat $CONF_FILE | grep stopped_node_from | cut -c 23-`
  
  if [ $((stopped_node_from)) -gt $((NODE_NUM)) ]; then
    echo -e "[ -INFO- ] All nodes will be in the state of Running."
  fi

#AUTOMATICALLY GENERATE DB passwords
  if [ ! -f $VAULTDIR/db_passwds.txt ]; then
    database_root_passwd=`openssl rand -base64 8`
    database_acct_passwd=`openssl rand -base64 8`
    echo -e "$database_root_passwd\n$database_acct_passwd" > $VAULTDIR/db_passwds.txt
  else
    database_root_passwd=`cat $VAULTDIR/db_passwds.txt | head -n1`
    database_acct_passwd=`cat $VAULTDIR/db_passwds.txt | tail -n1`
  fi

  if [ $HPC_USER_NUM_tmp = "*default*" ]; then
    HPC_USER_NUM=3
  elif [ ! -n $HPC_USER_NUM_tmp ]; then
    echo -e "[ -INFO- ] HPC_USER_NUM is not a number or '*default*', set to default value 3."
    HPC_USER_NUM=3
  else
    HPC_USER_NUM=$HPC_USER_NUM_tmp
  fi

  if [ $master_passwd = "*AUTOGEN*" ]; then
    master_passwd=`openssl rand -base64 8`
  fi
  if [ $compute_passwd = "*AUTOGEN*" ]; then
    compute_passwd=`openssl rand -base64 8`
  fi

  if [[ -n "$2" && $2 = 'newucid' ]]; then
    if [[ -n "$3" && $3 = "agpswd" ]]; then
      master_passwd=`openssl rand -base64 8`
      compute_passwd=`openssl rand -base64 8`
    fi
    if [ -f $VAULTDIR/UCID_LATEST.txt ]; then
      echo -e "[ -INFO- ] Creating a new Unique Cluster ID now..."
      prev_time_stamp=`cat $VAULTDIR/UCID_LATEST.txt | grep DATE: | cut -c 7-`
      prev_ucid=`cat $VAULTDIR/UCID_LATEST.txt | grep ID: | cut -c 5-`
      echo -e "$prev_time_stamp\t$prev_ucid" >> $VAULTDIR/UCID_ARCHIVE.txt
      time_curr=`date "+%Y-%m-%d-%H:%M:%S"`
      rand_str=`openssl rand -hex 4`
      ucid=$CLUSTER_ID$rand_str
      echo -e "[ -INFO- ] #NEW Unique Cluster ID created.\nDATE: $time_curr\nID: $ucid" > $VAULTDIR/UCID_LATEST.txt
    else
      echo -e "[ -INFO- ] UCID_LATEST.txt does not exist. Creating a Unique Cluster ID now..."
      time_curr=`date "+%Y-%m-%d-%H:%M:%S"`
      rand_str=`openssl rand -hex 4`
      ucid=$CLUSTER_ID$rand_str
      echo -e "[ -INFO- ] #NEW Unique Cluster ID created.\nDATE: $time_curr\nID: $ucid" > $VAULTDIR/UCID_LATEST.txt
    fi
  fi
 
  if [ ! -f $VAULTDIR/UCID_LATEST.txt ]; then
    echo -e "[ -INFO- ] UCID_LATEST.txt does not exist. Creating a Unique Cluster ID now..."
    time_curr=`date "+%Y-%m-%d-%H:%M:%S"`
    rand_str=`openssl rand -hex 4`
    ucid=$CLUSTER_ID$rand_str
    echo -e "[ -INFO- ] #NEW Unique Cluster ID created.\nDATE: $time_curr\nID: $ucid" > $VAULTDIR/UCID_LATEST.txt
  fi
  echo -e "$master_passwd\n$compute_passwd" > $VAULTDIR/root_passwds.txt

  ucid_str=`cat $VAULTDIR/UCID_LATEST.txt | grep ID: | cut -c 5-17`  
  echo -e "[ STEP 2 ] CLUSTER CONFIGURATION:\n\tCluster ID: $ucid_str\n\tRegion: $REGION_ID\n\tAvalability Zone: $ZONE_ID\n\t# of Nodes: $NODE_NUM\n\t# of Users: $HPC_USER_NUM\n\tMaster Node Root Password: (***sensitive***)\n\tCompute Node(s) Root Password: (***sensitive***)\n\tMaster Node Instance: $master_inst\n\tCompute Node(s) Instance: $compute_inst\n\tDatabase Node Instance: $database_inst\n\tOS Image: $os_image\n[ -INFO- ] Building you cluster now, this may take seconds ..."

# modify base file
  sed -i "s@DEFAULT_VPC_NAME@hpcstack_${ucid_str}@g" .hpc_stack.base
  sed -i "s@DEFAULT_SUBNET_NAME@hpcstack_${ucid_str}@g" .hpc_stack.base
  sed -i "s@DEFAULT_PUB_SUBNET_NAME@hpcstackpub_${ucid_str}@g" .hpc_stack.base
  sed -i "s@BLANK_ACCESS_KEY_ID@$access_key_id@g" .hpc_stack.base
  sed -i "s@BLANK_SECRET_KEY@$access_key_secret@g" .hpc_stack.base
  sed -i "s@BUCKET_ID@$ucid_str@g" .hpc_stack.base
  sed -i "s@BUCKET_ACCESS_POLICY@$ucid_str@g" .hpc_stack.base
  sed -i "s@BUCKET_USER_ID@$ucid_str@g" .hpc_stack.base
  sed -i "s@SECURITY_GROUP_PUBLIC@${ucid_str}public@g" .hpc_stack.base
  sed -i "s@SECURITY_GROUP_INTRA@${ucid_str}intra@g" .hpc_stack.base
  sed -i "s@SECURITY_GROUP_MYSQL@${ucid_str}mysql@g" .hpc_stack.base
  sed -i "s@NAS_ACCESS_GROUP_@${ucid_str}nag@g" .hpc_stack.base
  sed -i "s@DEFAULT_REGION_ID@$REGION_ID@g" .hpc_stack.base
  sed -i 's/DEFAULT_ZONE_ID/'$ZONE_ID'/g' .hpc_stack.base
  sed -i 's/DEFAULT_NAS_ZONE/'$NAS_ZONE'/g' .hpc_stack.base
  sed -i 's/DEFAULT_NODE_NUM/'$NODE_NUM'/g' .hpc_stack.base
  sed -i 's/DEFAULT_USER_NUM/'$HPC_USER_NUM'/g' .hpc_stack.base
  sed -i "s@DEFAULT_MASTERINI@$master_init_param@" .hpc_stack.base
  sed -i "s@DEFAULT_COMPUTEINI@$compute_init_param@" .hpc_stack.base
  sed -i "s@DEFAULT_MASTER_PASSWD@$master_passwd@g" .hpc_stack.base
  sed -i "s@DEFAULT_COMPUTE_PASSWD@$compute_passwd@g" .hpc_stack.base
  sed -i "s@DEFAULT_DB_ROOT_PASSWD@$database_root_passwd@g" .hpc_stack.base
  sed -i "s@DEFAULT_DB_ACCT_PASSWD@$database_acct_passwd@g" .hpc_stack.base

# modify master and compute file
  sed -i 's/DEFAULT_ZONE_ID/'$ZONE_ID'/g' .hpc_stack.master
  sed -i 's/MASTER_INST/'"$master_inst"'/g' .hpc_stack.master
  sed -i 's/CLOUD_FLAG/'$CLOUD_FLAG'/g' .hpc_stack.master
  sed -i 's/MASTER_BANDWIDTH/'"$master_bandwidth"'/g' .hpc_stack.master
  sed -i 's/OS_IMAGE/'$os_image'/g' .hpc_stack.master
  sed -i 's/DEFAULT_ZONE_ID/'$ZONE_ID'/g' .hpc_stack.compute
  sed -i 's/COMPUTE_INST/'$compute_inst'/g' .hpc_stack.compute
  sed -i 's/COMPUTE_BANDWIDTH/'$compute_bandwidth'/g' .hpc_stack.compute
  sed -i 's/OS_IMAGE/'$os_image'/g' .hpc_stack.compute
  sed -i 's/DEFAULT_ZONE_ID/'$ZONE_ID'/g' .hpc_stack.database
  sed -i 's/DB_INST/'$database_inst'/g' .hpc_stack.database
  sed -i 's/DATABASE_BANDWIDTH/'$database_bandwidth'/g' .hpc_stack.database
#  sed -i 's/OS_IMAGE/'$os_image'/g' .hpc_stack.database
  sed -i 's/DEFAULT_ZONE_ID/'$ZONE_ID'/g' .hpc_stack.natgw
  sed -i 's/MASTER_BANDWIDTH/'"$master_bandwidth"'/g' .hpc_stack.natgw
  
  for i in $(seq 1 $NODE_NUM )
  do
    /bin/cp .hpc_stack.compute .hpc_stack.compute${i}
    if [ $((i)) -ge $((stopped_node_from)) ]; then
      sed -i 's/RUNNING_FLAG/false/g' .hpc_stack.compute${i}
    else
      sed -i 's/RUNNING_FLAG/true/g' .hpc_stack.compute${i}
    fi
    sed -i 's/COMPUTE_NODE_N/compute'${i}'/g' .hpc_stack.compute${i}  
  done
  mv .hpc_stack.base hpc_stack_base.tf
  mv .hpc_stack.database hpc_stack_database.tf
  mv .hpc_stack.master hpc_stack_master.tf
  mv .hpc_stack.natgw hpc_stack_natgw.tf
  
  for i in $(seq 1 $NODE_NUM )
  do
    mv .hpc_stack.compute${i} hpc_stack_compute${i}.tf
  done
  rm -rf .hpc_stack*
  rm -rf NAS_Zones*.txt
  cd $STACKDIR && terraform init >> ${logfile_raw} 2>${logfile}
  if [ $? -ne 0 ]; then
    echo -e "[WARNING:] Cluster initialization encountered problems. Please check the logfile for details."
    exit
  fi
  cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
  if [ $? -ne 0 ]; then
    echo -e "[WARNING:] Cluster initialization encountered problems. Please check the logfile for details."
    exit
  fi
  /bin/cp hpc_stack_compute1.tf compute_template
  getstate
  bucket_name=`cat $STACKDIR/terraform.tfstate | grep -w \"bucket\" | head -n1 | awk -F"\"" '{print $4}'`
  bucket_secret_id=`cat $STACKDIR/terraform.tfstate | grep -w secret_id | awk -F"\"" '{print $4}'`
  bucket_access_secret=`cat $STACKDIR/terraform.tfstate | grep -w secret_key | awk -F"\"" '{print $4}'`
  root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
  master_address=`cat $STACKDIR/currentstate | head -n1`
  echo -e "This is the key pair for you to use the NowNetDisk.\nPowered by Shanghai HPC-Now Co., Ltd\n*** DO NOT share the key pair with any untrusted ones! \nSecret Key ID:\t\t$bucket_secret_id\nAccess Secret Key:\t$bucket_access_secret" > $VAULTDIR/bucket_secrets.txt
  sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "wget ${URL_ROOT_QCLOUD}cos.conf -O /root/.cos.conf -q && sed -i 's#BLANK_ACCESS_KEY#$bucket_secret_id#g' /root/.cos.conf && sed -i 's#BLANK_SECRET_KEY#$bucket_access_secret#g' /root/.cos.conf && sed -i 's#DEFAULT_REGION#$REGION_ID#g' /root/.cos.conf && sed -i 's#BLANK_BUCKET_NAME#$bucket_name#g' /root/.cos.conf && chmod 644 /root/.cos.conf"
  echo -e "HPC-NOW CLUSTER SUMMARY\nMaster Node IP: $master_address\nMaster Node Root Password: $root_passwd\n\nNetDisk Address: cos: $bucket_name\nNetDisk Region: $REGION_ID\nNetDisk AccessKey ID: $bucket_secret_id\nNetDisk Secret Key: $bucket_access_secret\n" > $VAULTDIR/_CLUSTER_SUMMARY.txt
  echo -e "[ STEP 2 ] Remote executing now ... " && rexec_connect 7 && rexec_all 8
  echo -e "[ -INFO- ] AFTER OPERATION:"
  graph
  echo -e "[ -DONE- ] Congratulations! The cluster is initializing now. This step may take at least 7 minutes. \n[ -INFO- ] After 7 minutes of waiting, you can log in the master node and manage your cluster.\n[ -INFO- ] IMPORTANT: By default, the initialization of the cluster includes building OpenMPI-3.1.6, OpenMPI-4.1.2, and MPICH-3.2.1.\n[ -INFO- ] Building these components needs extra 10 minutes.\n[ -INFO- ] Please check the initialization progress in the /root/cluster_init.log.\n[ -INFO- ] By default, NO HPC software will be built into the cluster. Please run 'hpcmgr install' command to install the software you need.\n"
  exit
fi

if [[ -n $1 && $1 = "init" && $CLOUD_STR = "aws" ]]; then
  if [[ -f $STACKDIR/currentstate || -f $STACKDIR/compute_template ]]; then
    echo -e "[ FATAL: ] It seems the cluster is already in place. Please empty your stack folder and retry. Exit now.\n"
    exit
  fi
  echo -e "[ START: ] Start initializing the cluster ... "
  ########## To make sure all the subdirs exist ######
  if [ ! -d $STACKDIR ]; then
    mkdir -p $STACKDIR
  fi
  if [ ! -d $VAULTDIR ]; then
    mkdir -p $VAULTDIR
  fi
  if [ ! -d $CONFDIR ]; then
    mkdir -p $CONFDIR
    if [ -f tf_prep.conf ]; then
      mv tf_prep.conf $CONFDIR
    fi
  fi
  if [ ! -d $LOGDIR ]; then
    mkdir -p $LOGDIR
  fi
  if [ ! -f $CONFDIR/tf_prep.conf ]; then
    echo -e "[ -INFO- ] IMPORTANT: No configure file found. Using the default configure file to initialize this cluster.\n[ -INFO- ] IMPORTANT: By default, the initialization process includes building OpenMPI-4.1.2, OpenMPI-3.1.6, and MPICH-3.2.1.\n[ -INFO- ] By default, NO HPC software will be built into the cluster. Please run 'hpcmgr install' command to install the software you need."
    wget ${URL_ROOT_AWS}tf_prep.conf -O $CONFDIR/tf_prep.conf -q
  fi
  if [[ -n $2 && $2 = 'global' ]]; then
    sed -i 's/cn-northwest-/us-east-/g' $CONFDIR/tf_prep.conf
    sed -i 's/cn-north-/us-east-/g' $CONFDIR/tf_prep.conf  
  fi
  #confirmation
  cd $STACKDIR
  echo -e "[ STEP 1 ] Creating input files now..."  
  rm -rf hpc_stack_*
  rm -rf compute_template
  wget ${URL_ROOT_AWS}hpc_stack_aws.base -O .hpc_stack.base -q
  wget ${URL_ROOT_AWS}hpc_stack_aws.master -O .hpc_stack.master -q
  wget ${URL_ROOT_AWS}hpc_stack_aws.master.eni -O hpc_stack_master_eni.tf -q
  wget ${URL_ROOT_AWS}hpc_stack_aws.compute -O .hpc_stack.compute -q
  wget ${URL_ROOT_AWS}hpc_stack_aws.compute.eni -O .hpc_stack.compute.eni -q
  wget ${URL_ROOT_AWS}hpc_stack_aws.database -O .hpc_stack.database -q
  wget ${URL_ROOT_AWS}hpc_stack_aws.database.eni -O hpc_stack_database_eni.tf -q
  wget ${URL_ROOT_AWS}hpc_stack_aws.natgw -O .hpc_stack.natgw -q
  

  if [[ ! -f .hpc_stack.base || ! -f .hpc_stack.master || ! -f .hpc_stack.compute || ! -f .hpc_stack.natgw || ! -f .hpc_stack.database ]]; then
    echo -e "[ FATAL: ] Input files download failed. Exit now.\n"
    exit
  fi
  access_key_id=`cat $VAULTDIR/.secrets.txt | head -n1 | awk '{print $1}'`
  access_key_secret=`cat $VAULTDIR/.secrets.txt | head -n2 | tail -n1 | awk '{print $1}'`
  CONF_FILE="$CONFDIR/tf_prep.conf"
  CLUSTER_ID=`cat $CONF_FILE | grep CLUSTER_ID | cut -c 23-28`
  REGION_ID=`cat $CONF_FILE | grep REGION_ID | cut -c 23-`
  ZONE_ID=`cat $CONF_FILE | grep ZONE_ID | cut -c 23-`
  NODE_NUM=`cat $CONF_FILE | grep NODE_NUM | cut -c 23-`
  HPC_USER_NUM_tmp=`cat $CONF_FILE | grep HPC_USER_NUM | cut -c 23-`
  master_init_param=`cat $CONF_FILE | grep master_init_param | cut -c 23-`
  compute_init_param=`cat $CONF_FILE | grep compute_init_param | cut -c 23-`
  master_passwd=`cat $CONF_FILE | grep master_passwd | cut -c 23-`
  compute_passwd=`cat $CONF_FILE | grep compute_passwd | cut -c 23-`
  master_inst=`cat $CONF_FILE | grep master_inst | cut -c 23-`
  compute_inst=`cat $CONF_FILE | grep compute_inst | cut -c 23-`
  os_image_raw=`cat $CONF_FILE | grep os_image | cut -c 23-`
  ht_flag=`cat $CONF_FILE | grep hyperthreading | cut -c 23-`
  echo -e "[ -INFO- ] All nodes will be in the state of Running."
  number_of_vcpu=`echo $compute_inst | awk -F"c" '{print $1}' | sed 's/[a-z]//g'`
  cpu_core_num=$((number_of_vcpu/2))
  if [ $ht_flag = 'OFF' ]; then
    THREADS=1
  elif [ $ht_flag = 'ON' ]; then
    THREADS=2
  fi
  echo $REGION_ID | grep cn >> /dev/null 2>&1
  if [ $? -eq 0 ]; then
    REGION_FLAG=cn_regions
    os_image=${os_image_raw}cn
    db_os_image=centos7cn
    nat_os_image=centos7cn
  else
    REGION_FLAG=global_regions
    os_image=${os_image_raw}global
    db_os_image=centos7global
    nat_os_image=centos7global
  fi

#AUTOMATICALLY GENERATE DB passwords
  if [ ! -f $VAULTDIR/db_passwds.txt ]; then
    database_root_passwd=`openssl rand -base64 8`
    database_acct_passwd=`openssl rand -base64 8`
    echo -e "$database_root_passwd\n$database_acct_passwd" > $VAULTDIR/db_passwds.txt
  else
    database_root_passwd=`cat $VAULTDIR/db_passwds.txt | head -n1`
    database_acct_passwd=`cat $VAULTDIR/db_passwds.txt | tail -n1`
  fi

  if [ $HPC_USER_NUM_tmp = "*default*" ]; then
    HPC_USER_NUM=3
  elif [ ! -n $HPC_USER_NUM_tmp ]; then
    echo -e "[ -INFO- ] HPC_USER_NUM is not a number or '*default*', set to default value 3."
    HPC_USER_NUM=3
  else
    HPC_USER_NUM=$HPC_USER_NUM_tmp
  fi

  if [ $master_passwd = "*AUTOGEN*" ]; then
    master_passwd=`openssl rand -base64 8`
  fi
  if [ $compute_passwd = "*AUTOGEN*" ]; then
    compute_passwd=`openssl rand -base64 8`
  fi

  if [[ -n "$2" && $2 = 'newucid' ]]; then
    if [[ -n "$3" && $3 = "agpswd" ]]; then
      master_passwd=`openssl rand -base64 8`
      compute_passwd=`openssl rand -base64 8`
    fi
    if [ -f $VAULTDIR/UCID_LATEST.txt ]; then
      echo -e "[ -INFO- ] Creating a new Unique Cluster ID now..."
      prev_time_stamp=`cat $VAULTDIR/UCID_LATEST.txt | grep DATE: | cut -c 7-`
      prev_ucid=`cat $VAULTDIR/UCID_LATEST.txt | grep ID: | cut -c 5-`
      echo -e "$prev_time_stamp\t$prev_ucid" >> $VAULTDIR/UCID_ARCHIVE.txt
      time_curr=`date "+%Y-%m-%d-%H:%M:%S"`
      rand_str=`openssl rand -hex 4`
      ucid=$CLUSTER_ID$rand_str
      echo -e "[ -INFO- ] #NEW Unique Cluster ID created.\nDATE: $time_curr\nID: $ucid" > $VAULTDIR/UCID_LATEST.txt
    else
      echo -e "[ -INFO- ] UCID_LATEST.txt does not exist. Creating a Unique Cluster ID now..."
      time_curr=`date "+%Y-%m-%d-%H:%M:%S"`
      rand_str=`openssl rand -hex 4`
      ucid=$CLUSTER_ID$rand_str
      echo -e "[ -INFO- ] #NEW Unique Cluster ID created.\nDATE: $time_curr\nID: $ucid" > $VAULTDIR/UCID_LATEST.txt
    fi
  fi
 
  if [ ! -f $VAULTDIR/UCID_LATEST.txt ]; then
    echo -e "[ -INFO- ] UCID_LATEST.txt does not exist. Creating a Unique Cluster ID now..."
    time_curr=`date "+%Y-%m-%d-%H:%M:%S"`
    rand_str=`openssl rand -hex 4`
    ucid=$CLUSTER_ID$rand_str
    echo -e "[ -INFO- ] #NEW Unique Cluster ID created.\nDATE: $time_curr\nID: $ucid" > $VAULTDIR/UCID_LATEST.txt
  fi
  echo -e "$master_passwd\n$compute_passwd" > $VAULTDIR/root_passwds.txt

  ucid_str=`cat $VAULTDIR/UCID_LATEST.txt | grep ID: | cut -c 5-17`  
  echo -e "[ STEP 2 ] CLUSTER CONFIGURATION:\n\tCluster ID: $ucid_str\n\tRegion: $REGION_ID\n\tAvalability Zone: $ZONE_ID\n\t# of Nodes: $NODE_NUM\n\t# of Users: $HPC_USER_NUM\n\tMaster Node Root Password: (***sensitive***)\n\tCompute Node(s) Root Password: (***sensitive***)\n\tMaster Node Instance: $master_inst\n\tCompute Node(s) Instance: $compute_inst\n\tOS Image: $os_image\n[ -INFO- ] Building you cluster now, this may take seconds ..."

# modify base file
  sed -i "s@DEFAULT_VPC_NAME@hpcstack_${ucid_str}@g" .hpc_stack.base
  sed -i "s@DEFAULT_SUBNET_NAME@hpcstack_${ucid_str}@g" .hpc_stack.base
  sed -i "s@DEFAULT_PUB_SUBNET_NAME@hpcstackpub_${ucid_str}@g" .hpc_stack.base
  sed -i "s@BLANK_ACCESS_KEY_ID@$access_key_id@g" .hpc_stack.base
  sed -i "s@BLANK_SECRET_KEY@$access_key_secret@g" .hpc_stack.base
  sed -i "s@BUCKET_ID@${ucid_str}@g" .hpc_stack.base
  sed -i "s@BUCKET_ACCESS_POLICY@${ucid_str}@g" .hpc_stack.base
  sed -i "s@BUCKET_USER_ID@$ucid_str@g" .hpc_stack.base
  sed -i "s@SECURITY_GROUP_PUBLIC@${ucid_str}public@g" .hpc_stack.base
  sed -i "s@SECURITY_GROUP_INTRA@${ucid_str}intra@g" .hpc_stack.base
  sed -i "s@SECURITY_GROUP_MYSQL@${ucid_str}mysql@g" .hpc_stack.base
  sed -i "s@NAS_ACCESS_GROUP_@${ucid_str}nag@g" .hpc_stack.base
  sed -i "s@DEFAULT_REGION_ID@$REGION_ID@g" .hpc_stack.base
  sed -i "s@RG_NAME@$hpcstack_${ucid_str}@g" .hpc_stack.base  
  sed -i 's/DEFAULT_ZONE_ID/'$ZONE_ID'/g' .hpc_stack.base
  sed -i 's/DEFAULT_NODE_NUM/'$NODE_NUM'/g' .hpc_stack.base
  sed -i 's/DEFAULT_USER_NUM/'$HPC_USER_NUM'/g' .hpc_stack.base
  sed -i "s@DEFAULT_MASTERINI@$master_init_param@" .hpc_stack.base
  sed -i "s@DEFAULT_COMPUTEINI@$compute_init_param@" .hpc_stack.base
  sed -i "s@DEFAULT_MASTER_PASSWD@$master_passwd@g" .hpc_stack.base
  sed -i "s@DEFAULT_COMPUTE_PASSWD@$compute_passwd@g" .hpc_stack.base
  sed -i "s@DEFAULT_DB_ROOT_PASSWD@$database_root_passwd@g" .hpc_stack.base
  sed -i "s@DEFAULT_DB_ACCT_PASSWD@$database_acct_passwd@g" .hpc_stack.base

# modify master and compute file
  sed -i 's/DEFAULT_ZONE_ID/'$ZONE_ID'/g' .hpc_stack.master
  sed -i 's/MASTER_INST/'"$master_inst"'/g' .hpc_stack.master
  sed -i 's/OS_IMAGE/'$os_image'/g' .hpc_stack.master
  sed -i 's/CLOUD_FLAG/'$CLOUD_FLAG'/g' .hpc_stack.master
  sed -i "s@RG_NAME@$hpcstack_${ucid_str}@g" .hpc_stack.master
  sed -i 's/DEFAULT_ZONE_ID/'$ZONE_ID'/g' .hpc_stack.compute
  sed -i 's/COMPUTE_INST/'$compute_inst'/g' .hpc_stack.compute
  sed -i 's/OS_IMAGE/'$os_image'/g' .hpc_stack.compute
  sed -i "s@RG_NAME@$hpcstack_${ucid_str}@g" .hpc_stack.compute
  sed -i "s@CPU_CORE_NUM@$cpu_core_num@g" .hpc_stack.compute
  sed -i "s@THREADS_PER_CORE@$THREADS@g" .hpc_stack.compute
  sed -i 's/DEFAULT_ZONE_ID/'$ZONE_ID'/g' .hpc_stack.database
  sed -i 's/DB_OS_IMAGE/'$db_os_image'/g' .hpc_stack.database
  sed -i "s@RG_NAME@$hpcstack_${ucid_str}@g" .hpc_stack.database
  sed -i 's/DEFAULT_ZONE_ID/'$ZONE_ID'/g' .hpc_stack.natgw
  sed -i 's/NAT_OS_IMAGE/'$nat_os_image'/g' .hpc_stack.natgw
  sed -i "s@RG_NAME@$hpcstack_${ucid_str}@g" .hpc_stack.natgw
  sed -i "s@RG_NAME@$hpcstack_${ucid_str}@g" .hpc_stack.compute.eni
  sed -i "s@RG_NAME@$hpcstack_${ucid_str}@g" hpc_stack_master_eni.tf
  sed -i "s@RG_NAME@$hpcstack_${ucid_str}@g" hpc_stack_database_eni.tf
  
  for i in $(seq 1 $NODE_NUM )
  do
    /bin/cp .hpc_stack.compute .hpc_stack.compute${i}
    /bin/cp .hpc_stack.compute.eni .hpc_stack.compute${i}.eni
    sed -i 's/COMPUTE_NODE_N/compute'${i}'/g' .hpc_stack.compute${i}
    sed -i 's/COMPUTE_NODE_N/compute'${i}'/g' .hpc_stack.compute${i}.eni   
  done
  mv .hpc_stack.base hpc_stack_base.tf
  mv .hpc_stack.database hpc_stack_database.tf
  mv .hpc_stack.master hpc_stack_master.tf
  mv .hpc_stack.natgw hpc_stack_natgw.tf
  
  for i in $(seq 1 $NODE_NUM )
  do
    mv .hpc_stack.compute${i} hpc_stack_compute${i}.tf
    mv .hpc_stack.compute${i}.eni hpc_stack_compute${i}_eni.tf
  done
  rm -rf .hpc_stack*
  cd $STACKDIR && terraform init >> ${logfile_raw} 2>${logfile}
  if [ $? -ne 0 ]; then
    echo -e "[WARNING:] Cluster initialization encountered problems. Please check the logfile for details."
    exit
  fi
  cd $STACKDIR && echo "yes" | terraform apply >> ${logfile_raw} 2>${logfile}
  if [ $? -ne 0 ]; then
    echo -e "[WARNING:] Cluster initialization encountered problems. Please check the logfile for details."
    exit
  fi
  /bin/cp hpc_stack_compute1.tf compute_template
  /bin/cp hpc_stack_compute1_eni.tf compute_eni_template
  getstate
  bucket_name=`cat $STACKDIR/terraform.tfstate | grep -w \"bucket\" | head -n1 | awk -F"\"" '{print $4}'`
  bucket_secret_id=`sed -n '/aws_iam_access_key/,+15p' $STACKDIR/terraform.tfstate | grep -w id | awk -F"\"" '{print $4}'`
  bucket_access_secret=`sed -n '/aws_iam_access_key/,+15p' $STACKDIR/terraform.tfstate | grep -w secret | awk -F"\"" '{print $4}'`
  echo -e "This is the key pair for you to use the NowNetDisk.\nPowered by Shanghai HPC-Now Co., Ltd\n*** DO NOT share the key pair with any untrusted ones! \nSecret Key ID:\t\t$bucket_secret_id\nAccess Secret Key:\t$bucket_access_secret" > $VAULTDIR/bucket_secrets.txt
  echo -e "[ STEP 2 ] Remote executing now ... " 
  for i in $( seq 1 120)
  do
	  sleep 1
  done
  root_passwd=`cat $VAULTDIR/root_passwds.txt | head -n1`
  master_address=`cat $STACKDIR/currentstate | head -n1`
  if [ $REGION_FLAG = 'cn_regions' ]; then
    s3endpoint=s3.$REGION_ID.amazonaws.com.cn
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "wget ${URL_ROOT_AWS}s3cfg.txt -O /root/.s3cfg -q && sed -i 's#BLANK_ACCESS_KEY#$bucket_secret_id#g' /root/.s3cfg && sed -i 's#BLANK_SECRET_KEY_ID#$bucket_access_secret#g' /root/.s3cfg && sed -i 's#DEFAULT_REGION#$REGION_ID#g' /root/.s3cfg && sed -i 's#DEFAULT_ENDPOINT#$s3endpoint#g' /root/.s3cfg && chmod 600 /root/.s3cfg"
  else
    s3endpoint=s3.amazonaws.com
    sshpass -p $root_passwd ssh -o StrictHostKeyChecking=no root@$master_address "wget ${URL_ROOT_AWS}s3cfg.txt -O /root/.s3cfg -q && sed -i 's#BLANK_ACCESS_KEY#$bucket_secret_id#g' /root/.s3cfg && sed -i 's#BLANK_SECRET_KEY_ID#$bucket_access_secret#g' /root/.s3cfg && sed -i 's#DEFAULT_REGION#$REGION_ID#g' /root/.s3cfg && sed -i 's#DEFAULT_ENDPOINT#$s3endpoint#g' /root/.s3cfg && sed -i 's#amazonaws.com.cn##amazonaws.com#g' /root/.s3cfg && chmod 600 /root/.s3cfg"
  fi
  echo -e "HPC-NOW CLUSTER SUMMARY\nMaster Node IP: $master_address\nMaster Node Root Password: $root_passwd\n\nNetDisk Address: s3://$bucket_name\nNetDisk Region: $REGION_ID\nNetDisk AccessKey ID: $bucket_secret_id\nNetDisk Secret Key: $bucket_access_secret\n" > $VAULTDIR/_CLUSTER_SUMMARY.txt
  rexec_connect 7 && rexec_all 8
  echo -e "[ -INFO- ] AFTER OPERATION:"
  graph
  echo -e "[ -DONE- ] Congratulations! The cluster is initializing now. This step may take at least 7 minutes. \n[ -INFO- ] After 7 minutes of waiting, you can log in the master node and manage your cluster.\n[ -INFO- ] IMPORTANT: By default, the initialization of the cluster includes building OpenMPI-3.1.6, OpenMPI-4.1.2, and MPICH-3.2.1.\n[ -INFO- ] Building these components needs extra 10 minutes.\n[ -INFO- ] Please check the initialization progress in the /root/cluster_init.log.\n[ -INFO- ] By default, NO HPC software will be built into the cluster. Please run 'hpcmgr install' command to install the software you need.\n"
  exit
fi