#!/bin/bash
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
cat /etc/ssh/sshd_confg | grep -w "PermitRootLogin yes" >> /dev/null
if [ $? -ne 0 ]; then
  echo -e "PermitRootLogin yes" >> /etc/ssh/sshd_config
fi
systemctl restart sshd
useradd hpc-now -m -s /bin/bash
yum -y install at
systemctl start atd && systemctl enable atd
echo "$1" | passwd hpc-now --stdin >> /dev/null 2>&1
id ec2-user >> /dev/null 2>&1
if [ $? -eq 0 ]; then
  echo "92kwoiduurow7ri4#pwo-3dd" | passwd ec2-user --stdin >> /dev/null 2>&1 #THIS IS JUST A WORKAROUND
fi
if [ ! -d /home/hpc-now ]; then
  mkdir -p /home/hpc-now && chown -R hpc-now:hpc-now /home/hpc-now
fi
if [ ! -d /etc/js/ops/.stk ]; then
  mkdir -p /etc/js/ops/.stk/ && chown -R hpc-now:hpc-now /etc/js/ops/.stk/
fi
openssl rand -base64 16 > /etc/js/ops/.stk/.base.txt
cat /etc/js/ops/.stk/.base.txt | passwd ec2-user --stdin >> /dev/null 2>&1
yum install -y yum-utils >> /dev/null 2>&1
yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo >> /dev/null 2>&1
yum -y install terraform >> /dev/null 2>&1
wget https://hpc-now-1308065454.cos.ap-guangzhou.myqcloud.com/utils/hpcopr -O /usr/bin/hpcopr -q && chmod +x /usr/bin/hpcopr
echo -e "\n*IMPORTANT!* This is a trial version of HPC-NOW Cluster. It will be expired in 7 days. When it expires, you need to destroy all the cloud resources manually.\n" > /home/hpc-now/README.txt
echo -e "WELCOME TO HPC-NOW CLUSTER OPERATOR NODE.\n\nNOW stands for:\nStart your HPC immediately\nNOW also stands for:\n  *N*o\n  *O*peration\n  *W*orkload\nHope you enjoy your HPC journey with HPC-NOW Services! \n\nShanghai HPC-Now Technologies Co., Ltd. All rights reserved. (2022)\ninfo@hpc-now.com\n" >> /home/hpc-now/README.txt
echo -e "Usage: hpcopr command_name param1\n" >> /home/hpc-now/README.txt
echo -e "Commands:\n" >> /home/hpc-now/README.txt
echo -e "0 . new       - Create a new cluster.\n1 . init      - (Re)initialize the whole cluster.\n2 . help      - show this information.\n3 . graph     - show the topology of your cluster.\n4 . rmc       - reinitialize the master and compute nodes.\n5 . rmcdb     - reinitialize the master, compute, and database nodes.\n6 . delmc     - delete the master and compute nodes from the cluster.\n7 . delmcdb   - delete the master, compute, and database nodes from the cluster.\n8 . delc      - delete all (if param1 is 'all') or part of (if param1 is a number less than the number of all the compute nodes) of the compute nodes.\n9 . addc      - add compute nodes to current cluster, number is specified by param1.\n10. shutdownc - shutdown all (if param1 is 'all') or part (if param1 is a number less than the number of all the compute nodes) of the compuute nodes.\n11. allupc    - turn on all the compute nodes.\n12. reconfc   - reconfigure the compute nodes (# of the cores and memory capacity).\n13. destroy   - destroy the whole cluster.\n14. conf      - download configuration template to modify and initialize cluster.\n15. monthpay  - change the payment mode from ondemand to monthly payment [ IRREVERSABLE!!! PLEASE MAKE SURE YOU DO WANT TO DO THIS OPERATION ].\n\n" >> /home/hpc-now/README.txt
echo -e "HPC NOW, START now ... to infinity!" >> /home/hpc-now/README.txt
chown -R hpc-now:hpc-now /home/hpc-now/README.txt
echo "rm -rf /usr/bin/opr_init" | at now + 1 minute
echo "rm -rf /usr/bin/hpcopr" | at now + 7 days