#!/bin/bash

# Shanghai HPC-NOW Technologies Co., Ltd
# All rights reserved, Year 2022
# https://www.hpc-now.com
# mailto: info@hpc-now.com 
# This script is used by 'hpcmgr' command to build *Gromacs* to HPC-NOW cluster.

if [ ! -d /hpc_apps ]; then
  echo -e "[ FATAL: ] The root directory /hpc_apps is missing. Installation abort. Exit now."
  exit
fi

#Make the packs path
if [ ! -d /opt/packs ]; then
  mkdir -p /opt/packs
fi
URL_ROOT=https://hpc-now-1308065454.cos.ap-guangzhou.myqcloud.com/
URL_PKGS=${URL_ROOT}packages/
time_current=`date "+%Y-%m-%d %H:%M:%S"`
logfile=/var/log/hpcmgr_install.log && echo -e "\n# $time_current INSTALLING GROMACS" >> ${logfile}
echo -e "[ -INFO- ] INSTALLING GROMACS-version 2022-2 now."
tmp_log=/tmp/hpcmgr_install.log
APP_ROOT=/hpc_apps
NUM_PROCESSORS=`cat /proc/cpuinfo| grep "processor"| wc -l`
yum -y install python3-devel >> ${tmp_log} 2>&1

if [ -f $APP_ROOT/gromacs2022/bin/gmx_mpi_d ]; then
  echo -e "[ FATAL: ] It seems GROMACS mpi version is already in place. If you do want to rebuild it, please emtpy $APP_ROOT/gromacs2022/ folder and retry. Exit now.\n"
  exit
fi

echo -e "[ -INFO- ] By *default*, GROMACS will be built with fftw3. if you'd like to build with MKL or its own fftpack, please contact us."
echo -e "[ -INFO- ] If you'd like to build your own version of GROMACS, please contact us by email info@hpc-now.com, or contact your technical support."
echo -e "[ -INFO- ] You can also go to the source code in /opt/packs/gromacs and build as you preferred. But this is not recommended."
echo -e "[ -INFO- ] Checking the Compilers now ..."
module ava -t | grep mpich >> /dev/null 2>&1
if [ $? -eq 0 ]; then
  mpi_version=`module ava -t | grep mpich | tail -n1 | awk '{print $1}'`
  module load $mpi_version
  echo -e "[ -INFO- ] GROMACS will be built with $mpi_version."
else
  module ava -t | grep openmpi >> /dev/null 2>&1
  if [ $? -eq 0 ]; then
    mpi_version=`module ava -t | grep openmpi | tail -n1 | awk '{print $1}'`
    module load $mpi_version
    echo -e "[ -INFO- ] GROMACS will be built with $mpi_version. However, we recommend MPICH-4 for compiling GROMACS."
  else
    echo -e "[ FATAL: ] No MPI version found. Please install mpi first. You can run 'hpcmgr install mpich4'. Exit now.\n"
    exit
  fi
fi

module ava -t | grep gcc-12.1.0 >> /dev/null 2>&1
if [ $? -eq 0 ]; then
  module load gcc-12.1.0
  gcc_v=gcc-12.1.0
  gcc_vnum=12
  systemgcc='false'
  echo -e "[ -INFO- ] GROMACS will be built with GNU C Compiler: $gcc_v"
else
  gcc_v=`gcc --version | head -n1`
  gcc_vnum=`echo $gcc_v | awk '{print $3}' | awk -F"." '{print $1}'`
  systemgcc='true'
  echo -e "[ -INFO- ] GROMACS will be built with GNU C Compiler: $gcc_v"
  if [ $gcc_vnum -lt 8 ]; then
    echo -e "[WARNING:] Your gcc version is too old. We *STRONGLY* recommend you install new version of gcc first."
    echo -e "[WARNING:] You can press keyboard 'Ctrl C' to stop current building process, and then run 'hpcmgr install gcc12' to build GCC-12.1.0 first."
    for i in $( seq 1 10)
    do
	    sleep 1
      echo -e "[WARNING:] Will start building GROMACS in $((11-i)) second(s) ..."
    done
  fi
fi

if [ ! -f $APP_ROOT/fftw3/bin/fftw-wisdom ]; then
  echo -e "[ -INFO- ] Building fftw3 now ... "
  rm -rf /hpc_apps/fftw3
  if [ ! -f /opt/packs/fftw-3.3.10.tar.gz ]; then
    wget ${URL_PKGS}fftw-3.3.10.tar.gz -q -O /opt/packs/fftw-3.3.10.tar.gz
  fi
  cd /opt/packs && tar zxf fftw-3.3.10.tar.gz
  cd /opt/packs/fftw-3.3.10 && ./configure --prefix=$APP_ROOT/fftw3 --enable-sse2 --enable-avx --enable-avx2 --enable-shared >> $tmp_log 2>${logfile}
  make -j$NUM_PROCESSORS >> $tmp_log 2>${logfile}
  make install >> $tmp_log 2>${logfile}
else
  echo -e "[ -INFO- ] fftw3 is already in place."
fi

echo -e "[ -INFO- ] Downloading and extracting source files ..."
if [ ! -f /opt/packs/gromacs-2022.2.tar.gz ]; then
  wget ${URL_PKGS}gromacs-2022.2.tar.gz -O /opt/packs/gromacs-2022.2.tar.gz -q
fi
rm -rf /opt/packs/gromacs-2022.2
cd /opt/packs && tar zxf gromacs-2022.2.tar.gz
echo -e "[ -INFO- ] Building GROMACS-2022 to $APP_ROOT/gromacs2022 now...This step may take minutes."
cd /opt/packs/gromacs-2022.2 && mkdir build && cd build
cmake .. -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DGMX_MPI=on -DCMAKE_INSTALL_PREFIX=/hpc_apps/gromacs2022 -DGMX_FFT_LIBRARY=fftw3 -DFFTW_LIBRARY=/hpc_apps/fftw3/lib/libfftw3.a -DFFTW_INCLUDE_DIR=/hpc_apps/fftw3/include -DGMX_DOUBLE=on >> ${tmp_log} 2>${logfile}
make -j $NUM_PROCESSORS >> ${tmp_log} 2>${logfile}
make install >> ${tmp_log} 2>${logfile}
cat /etc/profile | grep "alias gmxenv" >> /dev/null 2>&1
if [ $? -ne 0 ]; then
  echo -e "alias gmxenv='source $APP_ROOT/gromacs2022/bin/GMXRC'" >> /etc/profile
fi  
echo -e "[ -DONE- ] Congratulations! GROMACS-2022-2 has been build to $APP_ROOT/gromacs2022. Please run 'gmxenv' command to load the environment before using GROMACS."