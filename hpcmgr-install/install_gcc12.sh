#!/bin/bash

# Shanghai HPC-NOW Technologies Co., Ltd
# All rights reserved, Year 2022
# https://www.hpc-now.com
# mailto: info@hpc-now.com 
# This script is used by 'hpcmgr' command to build *GNU Compiler Collections-12.1.0* to HPC-NOW cluster.

if [ ! -d /hpc_apps ]; then
  echo -e "[ FATAL: ] The root directory /hpc_apps is missing. Installation abort. Exit now."
  exit
fi

URL_ROOT=https://hpc-now-1308065454.cos.ap-guangzhou.myqcloud.com/
URL_PKGS=${URL_ROOT}packages/

time_current=`date "+%Y-%m-%d %H:%M:%S"`
logfile=/var/log/hpcmgr_install.log && echo -e "\n# $time_current INSTALLING GNU C COMPILER - 12.1.0" >> ${logfile}
tmp_log=/tmp/hpcmgr_install.log
APP_ROOT=/hpc_apps
NUM_PROCESSORS=`cat /proc/cpuinfo| grep "processor"| wc -l`

if [ -f $APP_ROOT/gcc-12.1.0/bin/gcc ]; then
  echo -e "[ -INFO- ] It seems GNU C Compiler - Version 12.1.0 is in place.\n[ -INFO- ] If you really want to rebuild it. Please delete the gcc-12.1.0 folder and retry. Exit now.\n"
  echo -e "#%Module1.0\nprepend-path PATH $APP_ROOT/gcc-12.1.0/bin\nprepend-path LD_LIBRARY_PATH $APP_ROOT/gcc-12.1.0/lib64\n" > /etc/modulefiles/gcc-12.1.0
  exit
fi
if [ ! -d /opt/packs ]; then
  mkdir -p /opt/packs
fi
time_current=`date "+%Y-%m-%d %H:%M:%S"`
echo -e "[ START: ] $time_current Building GNU C Compiler - Version 12.1.0  now ... "
echo -e "[ STEP 1 ] $time_current Downloading and extracting source packages, this step usually takes minutes ... "
if [ ! -f /opt/packs/gcc-12.1.0-full.tar.gz ]; then
  wget ${URL_PKGS}gcc-12.1.0-full.tar.gz -q -O /opt/packs/gcc-12.1.0-full.tar.gz
fi
tar zvxf /opt/packs/gcc-12.1.0-full.tar.gz -C /opt/packs >> $tmp_log 2>&1
cd /opt/packs/gcc-12.1.0 && ./configure --prefix=$APP_ROOT/gcc-12.1.0 --disable-multilib >> $tmp_log 2>&1
time_current=`date "+%Y-%m-%d %H:%M:%S"`
echo -e "[ STEP 2 ] $time_current Making gcc-12.1.0 now, this step usually takes more than 2 hours with 8 cores..."
make -j$NUM_PROCESSORS >> $tmp_log 2>&1
time_current=`date "+%Y-%m-%d %H:%M:%S"`
echo -e "[ STEP 3 ] $time_current Installing gcc-12.1.0 now, this step is quick ..."
make install >> $tmp_log 2>&1
time_current=`date "+%Y-%m-%d %H:%M:%S"`
echo -e "[ STEP 4 ] $time_current Comgratulations! GCC-12.1.0 has been built."
echo -e "#%Module1.0\nprepend-path PATH $APP_ROOT/gcc-12.1.0/bin\nprepend-path LD_LIBRARY_PATH $APP_ROOT/gcc-12.1.0/lib64\n" > /etc/modulefiles/gcc-12.1.0
echo -e "# $time_current GCC-12.1.0 has been built." >> ${logfile}
