
HPC, start NOW ... to infinity! 

This is a project initiated and maintained by Shanghai HPC-Now Technologies Co., Ltd
If you are interested in this project, please contact us by:
- Email: info@hpc-now.com || wangzhenrong@hpc-now.com
- Tel | WeChat : (86) 13661536219
- QQ | WeChat : 495458966 | K495458966

We aim to developing a fully scalable, super easy to use, and maintenance-free High Performance Computing (HPC) platform on public cloud. The platform's name is NOW, which stands for:

- You can always start your high performance journey immediately, no need to wait, install, download, or scale ... We do all these annoying work for you.
- The platform gives you the experience of |N|o |O|peration |W|orkload. You focus on your innovation in your domain, please do not waste any time on underlying cluster management workload.

The whole platform relies heavily on Bash shell scripts here, and all the scripts are triggered by two C-language programs. Due to lack of time, we just put the scripts and related templates here, without enough notes for you to deeply understand the complex logic. If you DO want to participate into this project, please feel free to let me know.

Current Functions (2022/8/31)
- Public Cloud Vendors:
  -- AWS(China Regions)
  -- AliCloud (China Regions)
  -- TencentCloud (China regions)
- Resource Types:
  -- CPU instances (X86_64)
- HPC Software
  -- Compilers: GNU C Compiler - Version 8 & 12
  -- Libraries： FFTW-3
  -- Software： Lammps， OpenFOAM， Gromacs
Please specify the vm instance, e.g.: a64c128g means:

cpu core = 64 memory size = 128G.

Roadmap:
- More public cloud vendors:
  -- Azure
  -- Google cloud
  -- Oracle Public cloud
- More resource types:
  -- GPU resources management
- More HPC Software
  -- To be determined