#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc,char *argv[]) 
{
  FILE* fp,fp2;
  FILE* secrets_write=NULL;
  char access_key_id[64];
  char access_key_secrets[64];
  int i,start,end;
  int ak_length=0;
  int sk_length=0;
  int param1_length=0;
  int param2_length=0;
  int param3_length=0; 
  int base_length=0;
  int total_length=0;
  char* param1=argv[1];
  char* param2=argv[2];
  char* param3=argv[3];
  char* cmd_base="curl -s https://gitee.com/zhenrong-wang/now-cloud-cluster/raw/main/scripts/hpcopr-mcloud-dev.sh | bash -s ";
  char final_cmd[128];
  char input[3];
  char doubleconfirm[3];
  int param_num=argc-1;
  
  base_length=strlen(cmd_base);
  printf("\nHigh Performance Computing - start NOW!\n\nHPC-NOW Cluster Operator\n\nShanghai HPC-NOW Technologies Co., Ltd\nAll rights reserved (2022)\ninfo@hpc-now.com\nVersion 0.22.0810\n\n");
  
  for(i=0;i<3;i++){
    *(input+i)=' ';
    *(doubleconfirm+i)=' ';
  }
  
  if(param_num==1){
    param1_length=strlen(param1);
  }
  if(param_num==2){
    param1_length=strlen(param1);
    param2_length=strlen(param2);
  }
  if(param_num==3){
    param1_length=strlen(param1);
    param2_length=strlen(param2);
    param3_length=strlen(param3);
  }
  
  if(param1_length==0){
    printf("Usage: hpcopr command_name param1\n");
    printf("\nCommands:\n");
    printf("0 . new       - Create a new cluster.\n1 . init      - (Re)initialize the whole cluster.\n2 . help      - show this information.\n3 . graph     - show the topology of your cluster.\n4 . rmc       - reinitialize the master and compute nodes.\n5 . rmcdb     - reinitialize the master, compute, and database nodes.\n6 . delmc     - delete the master and compute nodes from the cluster.\n7 . delmcdb   - delete the master, compute, and database nodes from the cluster.\n8 . delc      - delete all (if param1 is 'all') or part of (if param1 is a number less than the number of all the compute nodes) of the compute nodes.\n9 . addc      - add compute nodes to current cluster, number is specified by param1.\n10. shutdownc - shutdown all (if param1 is 'all') or part (if param1 is a number less than the number of all the compute nodes) of the compuute nodes.\n11. allupc    - turn on all the compute nodes.\n12. reconfc   - reconfigure the compute nodes (# of the cores and memory capacity).\n13. destroy   - destroy the whole cluster.\n14. conf      - download configuration templaate to modify and initialize cluster.\n15. monthpay  - change the payment mode from ondemand to monthly payment [ IRREVERSABLE!!! PLEASE MAKE SURE YOU DO WANT TO DO THIS OPERATION ].\n");
    printf("\nHPC NOW, start now ... to infinity!\n\n");
    return -1;
  } 
  else{
    if(strcmp(param1,"new")==0){
      secrets_write=fopen("/tmp/.secrets.txt","w");
      if(secrets_write==NULL){
        printf("FATAL: Failed to create a critical file or folder.\n");
        return -1;
      }
      else{
        for(i=0;i<64;i++){
          *(access_key_id+i)=' ';
          *(access_key_secrets+i)=' ';
        }
        printf("[ -INFO- ] Please input your secrets key pair:\n[ INPUT: ] Access key ID:");
        scanf("%s",access_key_id);
        printf("[ INPUT: ] Access key secrets:");
        scanf("%s",access_key_secrets);
        ak_length=strlen(access_key_id);
        sk_length=strlen(access_key_secrets);  
        if(ak_length==24&&sk_length==30){
          fprintf(secrets_write,"%s\n%s\nCLOUD_A",access_key_id,access_key_secrets);
          fclose(secrets_write);
        }
        else if(ak_length==36&&sk_length==32){
          fprintf(secrets_write,"%s\n%s\nCLOUD_B",access_key_id,access_key_secrets);
          fclose(secrets_write);
        }
        else if(ak_length==20&&sk_length==40){
          fprintf(secrets_write,"%s\n%s\nCLOUD_C",access_key_id,access_key_secrets);
          fclose(secrets_write);
        }
        else
        {  
          printf("\n%d,%d\n%s,%s,\nFATAL: Invalid key pair. Please double check your inputs. Exit now.\n",ak_length,sk_length,access_key_id,access_key_secrets);
          fclose(secrets_write);
          system("rm -rf /tmp/.secrets.txt");
          return 1;
        }
        printf("[ -INFO- ] The secrets key pair has been stored.\n");
      }
      system("curl -s https://gitee.com/zhenrong-wang/now-cloud-cluster/raw/main/scripts/hpcopr-mcloud-dev.sh | bash -s new");
      return 0;
    }  
    else if(strcmp(param1,"rmc")==0||strcmp(param1,"rmcdb")==0||strcmp(param1,"delmc")==0||strcmp(param1,"delmcdb")==0||strcmp(param1,"delc")==0||strcmp(param1,"resume")==0||strcmp(param1,"addc")==0||strcmp(param1,"reconfc")==0||strcmp(param1,"shutdownc")==0||strcmp(param1,"allupc")==0||strcmp(param1,"destroy")==0||strcmp(param1,"init")==0||strcmp(param1,"repair")==0||strcmp(param1,"conf")==0||strcmp(param1,"monthpay")){
      printf("WARNING: You are operating the cluster, which may affect ALL the workloads, please input 'yes' to confirm your operation: ");
      scanf("%s",input);
      if(strcmp(input,"yes")!=0){
        printf("Only 'yes' is accepted to confirm. You chose to deny this operation. Nothing changed. Exit now.\n");
        return 1;
      }
      else{
        if(strcmp(param1,"destroy")==0){
          printf("\n********************************CAUTION!********************************\n\nYOU ARE DELETING THE WHOLE CLUSTER - INCLUDING ALL THE NODES AND *DATA*! \nTHIS OPERATION IS UNRECOVERABLE! \n\n********************************CAUTION!********************************\n\nARE YOU SURE? Only 'yes' is accepted to double confirm this operation: ");
          scanf("%s",doubleconfirm);
          if(strcmp(doubleconfirm,"yes")!=0){
            printf("Only 'yes' is accepted to confirm. You chose to deny this operation. Nothing changed. Exit now.\n");
            return 1;
          }
          else{
            printf("Operating started ...\n\n ");
          }
        }
        else{
          printf("Operating started ...\n\n ");
        }
      }
    }
    else if(strcmp(param1,"graph")==0||strcmp(param1,"help")==0||strcmp(param1,"emptytrash")==0||strcmp(param1,"updateconf")==0){
      printf("Operating started ...\n\n ");
    }
    else{
      printf("Invalid parameter. Exit now. Thanks for using HPC-NOW Cluster Service!\n");
      return 1;
    }
    for(i=0;i<128;i++){
      *(final_cmd+i)=' ';
    }
    for(i=0;i<base_length;i++){
      *(final_cmd+i)=*(cmd_base+i);
    }
    if(param1_length!=0&&param1_length<16){
      start=base_length;
      end=base_length+param1_length;
      for(i=start;i<end;i++){
        *(final_cmd+i)=*(param1+i-start);
      }
    }
    if(param2_length!=0&&param2_length<16){
      start=base_length+param1_length+1;
      end=base_length+param1_length+1+param2_length;
      for(i=start;i<end;i++){
        *(final_cmd+i)=*(param2+i-start);
      }
    }
    if(param3_length!=0&&param3_length<16){
      start=base_length+param1_length+1+param2_length+1;
      end=base_length+param1_length+1+param2_length+1+param3_length;
      for(i=start;i<end;i++){
        *(final_cmd+i)=*(param3+i-start);
      }
    }
    fp=popen(final_cmd,"w");
    pclose(fp);
    return 0; 
  }
}
